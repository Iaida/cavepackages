﻿using UnityEngine;

// based on example code from: https://docs.unity3d.com/ScriptReference/AudioClip.Create.html

[RequireComponent(typeof(AudioSource))]
public class DynamicAudioFrequency : MonoBehaviour
{
    public float Frequency;

    private int _samplePosition;
    private AudioClip _audioClip;
    private AudioSource _audioSource;
    private const int SampleRate = 44100;

    private void Awake()
    {
        _audioClip = AudioClip.Create("MySinusoid", SampleRate * 2, 1, SampleRate, true, OnAudioRead, OnAudioSetPosition);

        _audioSource = GetComponent<AudioSource>();
        _audioSource.clip = _audioClip;
        _audioSource.loop = true;

    }

    private void OnAudioRead(float[] data)
    {
        for (int i = 0; i < data.Length; i++)
        {
            data[i] = Mathf.Sin(2 * Mathf.PI * Frequency * _samplePosition / SampleRate);
            _samplePosition++;
        }
    }

    private void OnAudioSetPosition(int newPosition)
    {
        _samplePosition = newPosition;
    }


    [ContextMenu("Play")]
    public void Play()
    {
        _audioSource.Play();
    }
}
