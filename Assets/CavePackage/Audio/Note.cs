﻿// http://pages.mtu.edu/~suits/notefreqs.html
// https://pages.mtu.edu/~suits/NoteFreqCalcs.html

using System;
using UnityEngine;



// TODO: test negative octaves

public enum NoteName
{
    CNatural,
    CSharpDFlat,
    DNatural,
    DSharpEFlat,
    ENatural,
    FNatural,
    FSharpGFlat,
    GNatural,
    GSharpAFlat,
    ANatural,
    ASharpBFlat,
    BNatural
}

[Serializable]
public struct Note
{
    public NoteName Name;
    
    public int Octave;
    public float Frequency;

    
    private static readonly float A = Mathf.Pow(2.0f, 1.0f / 12.0f);

    public Note(NoteName name, int octave, float a4Frequency = 440.0f)
    {
        float c0Frequency = CalculateC0Frequency(a4Frequency);
        Name = name;
        Octave = octave;
        int n = octave * 12 + (int) name;
        Frequency = CalculateFrequencyNHalfStepsAway(c0Frequency, n);
    }

    public Note(float frequency, float a4Frequency = 440.0f)
    {
        float c0Frequency = CalculateC0Frequency(a4Frequency);
        int n = CalculateHalfStepsAwayFromFrequency(c0Frequency, frequency);
        // recalculate proper note frequency
        Frequency = CalculateFrequencyNHalfStepsAway(c0Frequency, n);

        if (n > 0)
        {
            // 12 half steps per octave
            Octave = n / 12;
            // index from 0 to 11 cast to enum
            Name = (NoteName) (n % 12);
        }
        else
        {
            // negative octave
            Octave = (n / 12) - 1;
            Name = (NoteName) (12 + (n % 12));
        }
    }

    public static float[] GenerateAllFrequencies(float a4Frequency = 440.0f)
    {
        float currentFrequency = CalculateC0Frequency(a4Frequency);
        float[] result = new float[108];
        for (int i = 0; i < result.Length; i++)
        {
            result[i] = currentFrequency;
            currentFrequency *= A;
        }
        return result;
    }

    private static float CalculateFrequencyNHalfStepsAway(float f0, int n)
    {
        return f0 * Mathf.Pow(A, n);
    }

    // inverse of CalculateFrequencyNHalfStepsAway
    private static int CalculateHalfStepsAwayFromFrequency(float f0, float f)
    {
        return Mathf.RoundToInt(Mathf.Log(f / f0, A));
    }

    private static float CalculateC0Frequency(float a4Frequency = 440.0f)
    {
        return CalculateFrequencyNHalfStepsAway(a4Frequency, -57);
    }

    public static float CalculateCNeg1Frequency(float a4Frequency = 440.0f)
    {
        return CalculateFrequencyNHalfStepsAway(a4Frequency, -69);
    }

};