﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PianoKeys : MonoBehaviour
{
    public Spectrum Spectrum;

    public GameObject WhiteKeyPrefab;
    public GameObject BlackKeyPrefab;

    public Note CurrentNote;

    private float _whiteKeyPressDepth;
    private float _blackKeyPressDepth;

    private float _whiteKeyDistance;
    private float _blackKeyDistance;



    // includes 88 Notes/Keys from A0 to C8
    private Dictionary<Note, GameObject> _noteToKey;

    private static readonly float A0Frequency = new Note(NoteName.ANatural, 0).Frequency;
    private static readonly float C8Frequency = new Note(NoteName.CNatural, 8).Frequency;

    

    private float _xPos;
    private float _lastWhiteXPos;

    private bool _currentNoteInitialized;

    // Use this for initialization
    private void Start()
    {
        _whiteKeyPressDepth = WhiteKeyPrefab.transform.lossyScale.y * 0.5f;
        _blackKeyPressDepth = BlackKeyPrefab.transform.lossyScale.y * 0.5f;

        _whiteKeyDistance = WhiteKeyPrefab.transform.lossyScale.x * 1.1f;
        _blackKeyDistance = _whiteKeyDistance / 2.0f;

        _xPos = transform.position.x;
        _lastWhiteXPos = transform.position.x;

        InitializeNoteToKeyMap();
    }

    // Update is called once per frame
    private void Update()
    {
        if (IsValidPianoFrequency(Spectrum.CurrentFrequency))
        {
            if (_currentNoteInitialized)
            {
                ReleaseNote(CurrentNote);
                CurrentNote = new Note(Spectrum.CurrentFrequency);
                PressNote(CurrentNote);
            }
            else
            {
                CurrentNote = new Note(Spectrum.CurrentFrequency);
                _currentNoteInitialized = true;
                PressNote(CurrentNote);
            }
        }
    }

    private void InitializeNoteToKeyMap()
    {
        _noteToKey = new Dictionary<Note, GameObject>
        {
            // start with cut off octave 0
            {new Note(NoteName.ANatural, 0), CreateKey(NoteName.ANatural, 0)},
            {new Note(NoteName.ASharpBFlat, 0), CreateKey(NoteName.ASharpBFlat, 0)},
            {new Note(NoteName.BNatural, 0), CreateKey(NoteName.BNatural, 0)}
        };

        // iterate through complete octaves
        for (int o = 1; o <= 7; o++)
        {
            foreach (var n in (NoteName[])Enum.GetValues(typeof(NoteName)))
            {
                _noteToKey.Add(new Note(n, o), CreateKey(n, o));
            }
        }

        // start with cut off octave 8
        _noteToKey.Add(new Note(NoteName.CNatural, 8), CreateKey(NoteName.CNatural, 8));
    }

    private static bool IsValidPianoFrequency(float frequency)
    {
        return frequency > A0Frequency && frequency < C8Frequency;
    }

    private GameObject CreateKey(NoteName noteName, int octave)
    {
        bool whiteKey = IsWhiteKey(noteName);
        GameObject prefabToInstantiate = whiteKey ? WhiteKeyPrefab : BlackKeyPrefab;
        if (whiteKey)
        {
            _xPos = _lastWhiteXPos + _whiteKeyDistance;
            _lastWhiteXPos = _xPos;
        }
        else
            _xPos += _blackKeyDistance;

        float yPos = transform.position.y + (whiteKey ? 0.0f : BlackKeyPrefab.transform.lossyScale.y * 1.5f);
        float zPos = transform.position.z + (whiteKey ? 0.0f : WhiteKeyPrefab.transform.lossyScale.z * 0.25f);

        GameObject instance = Instantiate(prefabToInstantiate,
            new Vector3(_xPos, yPos, zPos),
            Quaternion.identity,
            transform);

        instance.name = noteName.ToString() + octave;
        return instance;
    }

    private static bool IsWhiteKey(NoteName noteName)
    {
        return noteName.ToString().EndsWith("Natural");
    }


    private void PressNote(Note note)
    {
        YTranslation(_noteToKey[CurrentNote].transform,
            -(IsWhiteKey(note.Name) ? _whiteKeyPressDepth : _blackKeyPressDepth));
    }

    private void ReleaseNote(Note note)
    {
        YTranslation(_noteToKey[CurrentNote].transform,
            IsWhiteKey(note.Name) ? _whiteKeyPressDepth : _blackKeyPressDepth);
    }


    private static void YTranslation(Transform t, float yOffset)
    {
        t.position += new Vector3(0.0f, yOffset, 0.0f);
    }
}