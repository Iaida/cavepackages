﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;


[RequireComponent(typeof(AudioSource))]
public class Spectrum : MonoBehaviour
{
    public FFTWindow FftWindow = FFTWindow.BlackmanHarris;
    // must be a power of 2
    [Range(64, 8192)]
    public int NumberOfSamples = 512;
    public int AudioClipSampleRate;
    public float[] Data;
    public float CurrentFrequency;
    public Note CurrentNote;

    public bool Mute;
    public AudioMixerGroup MuteAudioMixerGroup;

    private AudioSource _audioSource;

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        Data = new float[NumberOfSamples];
        AudioClipSampleRate = _audioSource.clip.frequency;

        if (Mute)
            MuteAudioSource();

    }

    private void Update()
    {
        // https://docs.unity3d.com/ScriptReference/AudioSource.GetSpectrumData.html
        _audioSource.GetSpectrumData(Data, 0, FftWindow);
        CurrentFrequency = GetFundamentalFrequency();

        if (CurrentFrequency > 0)
            CurrentNote = new Note(CurrentFrequency);
    }

    // TODO other solution: https://answers.unity.com/questions/157940/getoutputdata-and-getspectrumdata-they-represent-t.html

    // based on: http://www.kaappine.fi/tutorials/fundamental-frequencies-and-detecting-notes/
    private float GetFundamentalFrequency()
    {
        float maxValue = Data.Max();
        int maxIndex = Array.IndexOf(Data, maxValue);

        // TODO : Test: AudioClipSampleRate OR AudioSettings.outputSampleRate ???
        return (float)maxIndex * AudioClipSampleRate / NumberOfSamples;
    }

    // muting audio source normally via toggle or volume stops 'AudioSource.GetSpectrumData' from working
    // https://answers.unity.com/questions/1462631/how-to-audiosourcegetspectrumdata-while-muted.html
    // 'Volume' set to -80 dB
    private void MuteAudioSource()
    {
        _audioSource.outputAudioMixerGroup = MuteAudioMixerGroup;
    }
}
