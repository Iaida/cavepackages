﻿using UnityEngine;

// visualizes 'Spectrum' component with 'Debug.DrawLine'

[RequireComponent(typeof(Spectrum))]
public class SpectrumVisualizerDebug : MonoBehaviour
{
    private Spectrum _spectrum;

    private void Awake()
    {
        _spectrum = GetComponent<Spectrum>();
    }

    private void Update()
    {
        // based on example code from: https://docs.unity3d.com/ScriptReference/AudioSource.GetSpectrumData.html

        for (int i = 1; i < _spectrum.Data.Length - 1; i++)
        {
            Debug.DrawLine(
                new Vector3(i - 1, _spectrum.Data[i] + 10, 0), 
                new Vector3(i, _spectrum.Data[i + 1] + 10, 0), 
                Color.red);
            Debug.DrawLine(
                new Vector3(i - 1, Mathf.Log(_spectrum.Data[i - 1]) + 10, 2), 
                new Vector3(i, Mathf.Log(_spectrum.Data[i]) + 10, 2), 
                Color.cyan);
            Debug.DrawLine(
                new Vector3(Mathf.Log(i - 1), _spectrum.Data[i - 1] - 10, 1), 
                new Vector3(Mathf.Log(i), _spectrum.Data[i] - 10, 1), 
                Color.green);
            Debug.DrawLine(
                new Vector3(Mathf.Log(i - 1), Mathf.Log(_spectrum.Data[i - 1]), 3), 
                new Vector3(Mathf.Log(i), Mathf.Log(_spectrum.Data[i]), 3), 
                Color.blue);

            Debug.DrawLine(
                new Vector3(i, _spectrum.Data[i] * _spectrum.NumberOfSamples, 4),
                new Vector3(i + 1, _spectrum.Data[i + 1] * _spectrum.NumberOfSamples, 4),
                Color.magenta);
        }
    }
}