﻿using UnityEngine;

// visualizes 'Spectrum' component with a 'LineRenderer' component

[RequireComponent(typeof(Spectrum))]
[RequireComponent(typeof(LineRenderer))]
public class SpectrumVisualizerLineRenderer : MonoBehaviour
{
    public float Length = 1;
    private Spectrum _spectrum;
    private LineRenderer _lineRenderer;

    private void Awake()
    {
        _spectrum = GetComponent<Spectrum>();
        _lineRenderer = GetComponent<LineRenderer>();
    }

    private void Update()
    {
        Vector3[] positions = DataToPositions(_spectrum.Data);
        _lineRenderer.positionCount = positions.Length;
        _lineRenderer.SetPositions(positions);
    }

    private Vector3[] DataToPositions(float[] data)
    {
        Vector3[] result = new Vector3[data.Length - 2];
        for (int i = 1; i < data.Length - 1; i++)
        {
            result[i-1] = new Vector3(((float)i / result.Length) * Length, Mathf.Log(data[i]), 0);
            if (float.IsNegativeInfinity(result[i - 1].y))
                result[i - 1].y = float.MinValue;

        }
        return result;
    }
}