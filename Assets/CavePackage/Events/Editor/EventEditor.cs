﻿// https://www.youtube.com/watch?v=raQ3iHhE_Kk
// https://github.com/roboryantron/Unite2017

using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GameEvent))]
public class EventEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GameEvent gameEvent = (GameEvent)target;

        GUI.enabled = Application.isPlaying;
   
        if (GUILayout.Button("Raise"))
            gameEvent.Raise();
    }
}
