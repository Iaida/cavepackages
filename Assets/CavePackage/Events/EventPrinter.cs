﻿using UnityEngine;

public class EventPrinter : MonoBehaviour
{
    private int _counter;

    public void Print()
    {
        Debug.Log((_counter++) + ": Custom Event Triggered");
    }
}
