﻿using UnityEngine;

public class FloorTransition : MonoBehaviour
{
    public GameEvent TransitionEvent;

    private float _lastHeight;
    

    // Use this for initialization
    private void Start ()
	{
	    _lastHeight = transform.position.y;
	}
	
	// Update is called once per frame
    private void Update ()
	{
	    float currentHeight = transform.position.y;
        // pos -> neg OR neg -> pos
	    if (_lastHeight > 0.0f && currentHeight < 0.0f || _lastHeight < 0.0f && currentHeight > 0.0f)
	    {
            Debug.Log("Event Raise");
	        TransitionEvent.Raise();
        }

	    _lastHeight = currentHeight;
	}
}
