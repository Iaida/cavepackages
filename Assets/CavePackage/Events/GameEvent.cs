﻿// https://www.youtube.com/watch?v=raQ3iHhE_Kk
// https://github.com/roboryantron/Unite2017

using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GameEvent : ScriptableObject
{
    private readonly List<GameEventListener> _eventListeners = new List<GameEventListener>();

    public void Raise()
    {
        foreach (GameEventListener listener in _eventListeners)
        {
            listener.OnEventRaised();
        }
    }

    public void RegisterListener(GameEventListener listener)
    {
        if (!_eventListeners.Contains(listener))
            _eventListeners.Add(listener);
    }

    public void UnregisterListener(GameEventListener listener)
    {
        if (_eventListeners.Contains(listener))
            _eventListeners.Remove(listener);
    }
}
