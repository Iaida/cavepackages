﻿using UnityEngine;

public class CustomBalanceBoard : MonoBehaviour
{
    [Range(-1.0f, 1.0f)]
    public float Horizontal;
    [Range(-1.0f, 1.0f)]
    public float Vertical;

    [Header("Input Settings")]
    // changed second pair of 'Horizontal' and 'Vertical' in 'Project Settings -> Input' to 'HorizontalJoy' and 'VerticalJoy'
    // this helps to differentiate between arrow keys + wasd axes and joystick axes
    public string HorizontalAxisName = "HorizontalJoy";
    public string VerticalAxisName = "VerticalJoy";


    private void Start ()
    {
        foreach (string joystickName in Input.GetJoystickNames())
        {
            Debug.Log("Joystick '" + joystickName + "' connected");
        }
    }

    private void Update ()
	{
        Horizontal = Input.GetAxis(HorizontalAxisName);
        Vertical = Input.GetAxis(VerticalAxisName);
    }
}
