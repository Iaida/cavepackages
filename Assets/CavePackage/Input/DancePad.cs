﻿using System;
using UnityEngine;

public class DancePad : MonoBehaviour
{
    public enum DancePadButton
    {
        Left = 0,
        Down = 1,
        Up = 2,
        Right = 3,
        Triangle = 4,
        Square = 5,
        Cross = 6,
        Circle = 7,
        Start = 8,
        Select = 9
    }

    private void Start ()
    {
	    foreach (string joystickName in Input.GetJoystickNames())
	    {
	        Debug.Log("Joystick " + joystickName + " connected");
        }
	}
	
    // for debugging purposes
    private void Update ()
	{
	    foreach (DancePadButton button in (DancePadButton[])Enum.GetValues(typeof(DancePadButton)))
	    {
	        if (GetButtonPressed(button))
                Debug.Log("Button " + button + " pressed");

	        if (GetButtonReleased(button))
	            Debug.Log("Button " + button + " released");
        }
    }

    public bool GetButtonPressed(DancePadButton button)
    {
        return Input.GetKeyDown("joystick button " + (int)button);
    }

    public bool GetButtonDown(DancePadButton button)
    {
        return Input.GetKey("joystick button " + (int)button);
    }

    public bool GetButtonReleased(DancePadButton button)
    {
        return Input.GetKeyUp("joystick button " + (int)button);
    }
}
