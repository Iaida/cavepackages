﻿using UnityEngine;

public static class CaveDimensions
{
    /// Cave and tracking coordinate system:
    /// x is cave width, left is negative, right is positive
    /// y is cave depth, entrance is negative, front screen plane is positive
    /// z is cave height, 0 is floor, ceiling is positive 2500
    /// values map to real world millimeters,
    /// example: value of 1000.0f -> 1000 mm, 100 cm, 1 m
    
    public const float EdgeLength = 2500;

    // DTrack bounds
    private static readonly Vector3 MinCoordinatesRaw = new Vector3(-1250, -1250, 0);
    private static readonly Vector3 MaxCoordinatesRaw = new Vector3(1250, 1250, 2500);
    private static readonly Vector3 MidCoordinatesRaw = (MinCoordinatesRaw + MaxCoordinatesRaw) / 2;

    // Unity bounds
    public static Vector3 MinCoordinates = TrackingToUnityPosition(MinCoordinatesRaw);
    public static Vector3 MaxCoordinates = TrackingToUnityPosition(MaxCoordinatesRaw);
    public static Vector3 MidCoordinates = TrackingToUnityPosition(MidCoordinatesRaw);
    
    private static readonly Matrix4x4 SwapMatrix12 = MathUtilities.GenerateSwapMatrix(1, 2);

    public static Vector3 TrackingToUnityPosition(Vector3 trackingPosition)
    {
        // swap y and z components
        return new Vector3(trackingPosition.x, trackingPosition.z, trackingPosition.y);
    }

    public static Matrix4x4 TrackingToUnityRotationMatrix(Matrix4x4 trackingRotMatrix)
    {
        // using matrices for swapping rows and columns: 
        // https://unapologetic.wordpress.com/2009/08/27/elementary-row-and-column-operations/

        // swap second and third row:       _swapMatrix12 * trackingRotMatrix 
        // swap second and third column:    trackingRotMatrix * _swapMatrix12
        // apply both:
        return SwapMatrix12 * trackingRotMatrix * SwapMatrix12;
    }

    public static Quaternion TrackingToUnityRotation(Quaternion trackingRotation)
    {
        return new Quaternion(
            trackingRotation.x, 
            trackingRotation.z, 
            trackingRotation.y, 
            -trackingRotation.w);
    }
}