﻿using System;
using UnityEditor;


// Custom Inspector of 'TrackerObject' for visibility of members dependent on the ScalingMode enum value of 'CurrentScalingMode'

[CustomEditor(typeof(TrackerObject))]
public class TrackerObjectInspector : Editor
{
    private TrackerObject _tos;

    private SerializedProperty _scriptProperty;
    private SerializedProperty _idProperty;
    private SerializedProperty _toggleChildrenProperty;
    private SerializedProperty _updatePositionProperty;
    private SerializedProperty _updateRotationProperty;
    private SerializedProperty _foundProperty;

    private SerializedProperty _nearAnyCaveFaceProperty;
    private SerializedProperty _warningDistanceProperty;

    private SerializedProperty _currentScalingModeProperty;
    private SerializedProperty _scalingFactorProperty;
    private SerializedProperty _scaledMinPositionModeProperty;
    private SerializedProperty _scaledMaxPositionModeProperty;


    private void OnEnable()
    {
        _tos = (TrackerObject)target;
        _scriptProperty = serializedObject.FindProperty("m_Script");
        _idProperty = serializedObject.FindProperty("Id");
        _toggleChildrenProperty = serializedObject.FindProperty("ToggleChildren");
        _updatePositionProperty = serializedObject.FindProperty("UpdatePosition");
        _updateRotationProperty = serializedObject.FindProperty("UpdateRotation");
        _foundProperty = serializedObject.FindProperty("Found");
        _nearAnyCaveFaceProperty = serializedObject.FindProperty("NearAnyCaveFace");
        _warningDistanceProperty = serializedObject.FindProperty("WarningDistance");

        _currentScalingModeProperty = serializedObject.FindProperty("CurrentScalingMode");
        _scalingFactorProperty = serializedObject.FindProperty("ScalingFactor");
        _scaledMinPositionModeProperty = serializedObject.FindProperty("ScaledMinPosition");
        _scaledMaxPositionModeProperty = serializedObject.FindProperty("ScaledMaxPosition");

    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUI.BeginDisabledGroup(true);
        EditorGUILayout.PropertyField(_scriptProperty, true);
        EditorGUI.EndDisabledGroup();

        EditorGUILayout.PropertyField(_idProperty);
        EditorGUILayout.PropertyField(_toggleChildrenProperty);
        EditorGUILayout.PropertyField(_updatePositionProperty);
        EditorGUILayout.PropertyField(_updateRotationProperty);

        EditorGUILayout.PropertyField(_foundProperty);

        EditorGUILayout.PropertyField(_nearAnyCaveFaceProperty);
        EditorGUILayout.PropertyField(_warningDistanceProperty);


        EditorGUILayout.PropertyField(_currentScalingModeProperty);

        // show variables in inspector depending on 'CurrentScalingMode' enum value: 
        // https://answers.unity.com/questions/192895/hideshow-properties-dynamically-in-inspector.html

        using (var group = new EditorGUILayout.FadeGroupScope(Convert.ToSingle(_tos.CurrentScalingMode == TrackerObject.ScalingMode.Factor)))
        {
            if (group.visible)
            {
                EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(_scalingFactorProperty);
                EditorGUI.indentLevel--;
            }
        }

        using (var group = new EditorGUILayout.FadeGroupScope(Convert.ToSingle(_tos.CurrentScalingMode == TrackerObject.ScalingMode.MinMaxVectors)))
        {
            if (group.visible)
            {
                EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(_scaledMinPositionModeProperty);
                EditorGUILayout.PropertyField(_scaledMaxPositionModeProperty);
                EditorGUI.indentLevel--;
            }
        }

        serializedObject.ApplyModifiedProperties();
    }
}
