﻿using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public struct Tracker
{
    public int Id;
    public Vector3 Position; 
    public Quaternion Rotation;
    public Matrix4x4 RotationMatrix;

    // assumes 'toAverage' contains only trackers with same Ids
    public Tracker(List<Tracker> toAverage)
    {
        // initialize identity element for Tracker addition
        Tracker accumulator = new Tracker{ Id = toAverage[0].Id };
        foreach (Tracker t in toAverage)
        {
            accumulator.RotationMatrix = MathUtilities.AddMatrices(accumulator.RotationMatrix, t.RotationMatrix);
            accumulator.Position += t.Position;
        }

        Id = accumulator.Id;
        Position = accumulator.Position / toAverage.Count;
        RotationMatrix = MathUtilities.ScaleMatrixComponents(accumulator.RotationMatrix, 1.0f / toAverage.Count);
        // Computing the average of a quaternion would be too costly:
        // https://stackoverflow.com/questions/12374087/average-of-multiple-quaternions/29315869
        // converting average of 'RotationMatrix' to quaternion instead
        Rotation = RotationMatrix.rotation;
    }


    public Tracker TrackingToUnity()
    {
        return new Tracker
        {
            Id = Id,
            Position = CaveDimensions.TrackingToUnityPosition(Position),
            Rotation = CaveDimensions.TrackingToUnityRotation(Rotation),
            RotationMatrix = CaveDimensions.TrackingToUnityRotationMatrix(RotationMatrix)
        };
    }
}