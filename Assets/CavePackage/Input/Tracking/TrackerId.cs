﻿// used as:
// - member in 'TrackerObject',
// - to search for a specific 'Tracker' within 'TrackingData'
// - to set the correct id for the ShutterGlasses in 'StereoRendering'

public enum TrackerId
{
    Wiimote = 0,
    Wristband2 = 2,
    ShutterGlasses = 3,
    Wristband4 = 4,
    Kart6 = 6
};