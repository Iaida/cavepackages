﻿using UnityEngine;

public class TrackerObject : MonoBehaviour
{
    public enum ScalingMode
    {
        None,
        Factor,
        MinMaxVectors
    }

    public TrackerId Id;
    [Tooltip("Toggles child objects based on detection of the tracker")]
    public bool ToggleChildren = true;

    public bool UpdatePosition = true;
    public bool UpdateRotation = true;

    public ScalingMode CurrentScalingMode = ScalingMode.Factor;
    public float ScalingFactor = 0.001f;
    public Vector3 ScaledMinPosition;
    public Vector3 ScaledMaxPosition;

    public bool Found;

    [Tooltip("Can be used to warn immersed users")]
    public bool NearAnyCaveFace;
    public float WarningDistance = 300.0f;

    private TrackingInputAsync _trackingInputAsync;

    private void Start()
    {
        if (TrackingInputAsync.Instance == null)
        {
            enabled = false;
            return;
        }

        _trackingInputAsync = TrackingInputAsync.Instance;
    }

    private void Update()
    {
        Tracker? optionalTracker = _trackingInputAsync.TrackingData.GetTracker(Id);
        Found = optionalTracker.HasValue;

        if (optionalTracker.HasValue)
        {
            UpdateWithTracker(optionalTracker.Value);
            NearAnyCaveFace = IsNearAnyCaveFace();
        }
        if (ToggleChildren)
            SetChildrenActive(Found);
    }


    private void UpdateWithTracker(Tracker tracker)
    {
        if (UpdatePosition)
        {
            switch (CurrentScalingMode)
            {
                case ScalingMode.None:
                    transform.localPosition = tracker.Position;
                    break;
                case ScalingMode.Factor:
                    transform.localPosition = tracker.Position * ScalingFactor;
                    break;
                case ScalingMode.MinMaxVectors:
                    transform.localPosition = MathUtilities.ScaleVector(tracker.Position,
                        CaveDimensions.MinCoordinates, CaveDimensions.MaxCoordinates,
                        ScaledMinPosition, ScaledMaxPosition);
                    break;
            }
        }

        if (UpdateRotation)
            transform.localRotation = tracker.Rotation;
    }

    private void SetChildrenActive(bool active)
    {
        // https://answers.unity.com/questions/205391/how-to-get-list-of-child-game-objects.html
        foreach (Transform childTransform in transform)
        {
            childTransform.gameObject.SetActive(active);
        }
    }

    private bool IsNearAnyCaveFace()
    {
        Vector3 currentLocalPos = transform.localPosition;
        return IsNearFront(currentLocalPos.z) || IsNearRight(currentLocalPos.x) || IsNearLeft(currentLocalPos.x);
    }

    private bool IsNearFront(float localZPosition)
    {
        return localZPosition > CaveDimensions.MaxCoordinates.z - WarningDistance;
    }

    private bool IsNearRight(float localXPosition)
    {
        return localXPosition > CaveDimensions.MaxCoordinates.x - WarningDistance;
    }

    private bool IsNearLeft(float localXPosition)
    {
        return localXPosition < CaveDimensions.MinCoordinates.x + WarningDistance;
    }

    // for wrist tracker swapping
    public static void SwapIds(TrackerObject a, TrackerObject b) 
    {
        TrackerId tmp = a.Id;
        a.Id = b.Id;
        b.Id = tmp;
    }
}
