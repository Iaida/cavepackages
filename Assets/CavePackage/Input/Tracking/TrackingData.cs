﻿using System.Collections.Generic;


[System.Serializable]
public struct TrackingData
{
    public uint Frame;
    public float Timestamp;
    // map with ids as keys would be preferable, but maps cannot be displayed within unity's inspector
    public List<Tracker> Trackers;


    public TrackingData(CircularBuffer<TrackingData> toAverage)
    {
        Frame = 0;
        Timestamp = 0.0f;

        // compute simple average of scalar members frame and Timestamp
        //TrackingData accumulator = new TrackingData();
        foreach (TrackingData td in toAverage)
        {
            Frame += td.Frame;
            Timestamp += td.Timestamp;
        }

        Frame = (uint) (Frame / toAverage.Count);
        Timestamp = Timestamp / toAverage.Count;

        // compute average of trackers
        var idTrackersMap = new Dictionary<int, List<Tracker>>();
        // iterate through input container and add 'Tracker' elements to a List corresponding to their 'Id'
        foreach (TrackingData td in toAverage)
        {
            foreach (Tracker t in td.Trackers)
            {
                // check if Id already present
                if (idTrackersMap.ContainsKey(t.Id))
                {
                    // add Tracker t to list belonging to its id
                    idTrackersMap[t.Id].Add(t);
                }
                else
                {
                    // create new entry for map with the new id and a list containing only Tracker t
                    idTrackersMap.Add(t.Id, new List<Tracker>() {t});
                }
            }
        }

        // convert the list of 'Tracker' objects to a single averaged 'Tracker'
        var idTrackerMap = new Dictionary<int, Tracker>();
        foreach (KeyValuePair<int, List<Tracker>> idTrackerListPair in idTrackersMap)
        {
            Tracker average = new Tracker(idTrackerListPair.Value);
            idTrackerMap.Add(idTrackerListPair.Key, average);
        }

        // convert the map to a list for readability within the inspector
        Trackers = new List<Tracker>();
        Trackers.AddRange(idTrackerMap.Values);
    }


    public Tracker? GetTracker(int id)
    {
        int index = Trackers.FindIndex(t => t.Id == id);

        if (index == -1)
            return null;

        return Trackers[index];
    }

    public Tracker? GetTracker(TrackerId id)
    {
        return GetTracker((int) id);
    }
    
    public void TrackingToUnity()
    {
        Trackers = Trackers.ConvertAll(t => t.TrackingToUnity());
    }
}