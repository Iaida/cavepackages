﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

public class TrackingInputAsync : MonoBehaviour
{
    public CaveSettings CaveSettings;
    public CaveMode OptionsParser;
    public bool UseMulticastAddress = true;
    public TrackingData TrackingData;
    public int MaxHistoryElements = 5;
    public bool MeasureHz;
    public int Hz;

    private UdpClient _client;
    private IPEndPoint _endPoint;
    private CircularBuffer<TrackingData> _trackingDataHistory;
    private string _trackingString;
    private int _hzCounter;


    // https://gamedev.stackexchange.com/questions/116009/in-unity-how-do-i-correctly-implement-the-singleton-pattern
    public static TrackingInputAsync Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        else
            Instance = this;
    }

    private void Start()
    {
        _trackingDataHistory = new CircularBuffer<TrackingData>(MaxHistoryElements);

        // Channels 1-4 within the DTrack software should be for cave clients, 5th channel for servers which use multicast
        if (OptionsParser.IsInCaveMode())
            UseMulticastAddress = false;

        if (UseMulticastAddress)
        {
            _client = new UdpClient(CaveSettings.TrackingMulticastPort)
            {
                // 'ReceiveTimeout' default would be infinite
                Client = { ReceiveTimeout = 1000 }
            };

            _client.JoinMulticastGroup(IPAddress.Parse(CaveSettings.TrackingMulticastIp));
            _endPoint = new IPEndPoint(IPAddress.Parse(CaveSettings.TrackingMulticastIp), CaveSettings.TrackingMulticastPort);
        }
        else
        {
            _client = new UdpClient(CaveSettings.TrackingPort)
            {
                // 'ReceiveTimeout' default would be infinite
                Client = { ReceiveTimeout = 1000 }
            };
            _endPoint = new IPEndPoint(IPAddress.Parse(CaveSettings.TrackingIp), CaveSettings.TrackingPort);
        }

        if (CheckForData())
        {
            try
            {
                // https://stackoverflow.com/questions/7266101/receive-messages-continuously-using-udpclient/7423947#7423947
                _client.BeginReceive(UpdateTracking, null);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }
        else
        {
            Debug.LogError("UDP check for tracking port " + CaveSettings.TrackingPort + " failed, disabling TrackingInputAsync");
            enabled = false;    
        }

        if (MeasureHz)
            InvokeRepeating("UpdateHz", 0, 1);
    }

    private void OnDestroy()
    {
        if (enabled && _client != null)
            _client.Close();
    }


    private bool CheckForData()
    {
        try
        {
            // throws exception after 'ReceiveTimeout' milliseconds if no data was received
            _client.Receive(ref _endPoint);
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    // AsyncCallBack for 'UdpClient.BeginReceive'
    private void UpdateTracking(IAsyncResult result)
    {
        byte[] received = _client.EndReceive(result, ref _endPoint);
        _client.BeginReceive(UpdateTracking, null);

        _trackingString = Encoding.UTF8.GetString(received, 0, received.Length);
        ProcessTrackingString();
    }

    private void ProcessTrackingString()
    {
        TrackingData newestTrackingData = TrackingStringParsing.Parse(_trackingString);
        
        newestTrackingData.TrackingToUnity();

        // add to history
        _trackingDataHistory.Add(newestTrackingData);
       
        // calculate average of history with custom 'TrackingData' constructor
        TrackingData = new TrackingData(_trackingDataHistory);

        if (MeasureHz)
            _hzCounter++;
    }


    private void UpdateHz()
    {
        Hz = _hzCounter;
        _hzCounter = 0;
    }
}