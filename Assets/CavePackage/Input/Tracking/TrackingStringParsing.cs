﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Debug = System.Diagnostics.Debug;


public static class TrackingStringParsing
{
    /// delimiter between lines,
    /// Carriage Return followed by Line Feed
    /// http://donsnotes.com/tech/charsets/ascii.html#EOL
    private const string LineSeparator = "\x0D\x0A";

    /// space character between components of a line
    private const char DataSeparator = ' ';

    /// converts a raw string from DTrack to TrackingData
    /// \param dataString input string to be parsed
    /// \return parsed TrackingData struct
    public static TrackingData Parse(string dataString)
    {
        TrackingData trackingData = new TrackingData();
        string[] lines = StringUtilities.Split(dataString, LineSeparator);

        foreach (string line in lines)
        {
            if (string.IsNullOrEmpty(line))
                continue;

            string[] splitLine = StringUtilities.Split(line, DataSeparator);

            switch (splitLine[0])
            {
                case "fr":
                    // This line contains frame count.
                    // First element is "fr", second element is the frame count represented as an integer
                    trackingData.Frame = uint.Parse(splitLine[1]);
                    break;
                case "ts":
                    // This line contains a timestamp.
                    // First element is "ts", second element is the timestamp represented as a float
                    trackingData.Timestamp = StringUtilities.StringToFloat(splitLine[1]);
                    break;
                case "6d":
                    // This line contains multiple arrays of tracking data.
                    // First element is "6d" followed by the number of tracked bodies
                    trackingData.Trackers = ParseTrackerLine(line, uint.Parse(splitLine[1]));
                    break;
            }
        }

        return trackingData;
    }

    /// converts the tracker line contained in raw DTrack string to a vector of Tracker structs
    /// \param dataString input string to be parsed
    /// \param trackedBodies number of trackers to be parsed
    /// \return resulting vector of parsed Tracker structs
    private static List<Tracker> ParseTrackerLine(string dataString, uint trackedBodies)
    {
        // check if trackers present
        if (trackedBodies == 0)
            return new List<Tracker>();

        // discard indicator and number of bodies
        dataString = dataString.Substring(5);

        // split at each space or square bracket
        string[] splitData = StringUtilities.Split(dataString, new[] { ' ', '[', ']' });

        // id + qual + pos(3) + rot(3) + rotmat(9)
        const int elementsPerTracker = 17;

        // check 'splitData' for the correct number of elements
        if (splitData.Length % elementsPerTracker != 0)
            return new List<Tracker>();

        // 'splitData' only contains floats represented as strings
        // convert string array to float array
        float[] floatData = Array.ConvertAll(splitData, StringUtilities.StringToFloat);

        // initialize with known capacity
        List<Tracker> trackers = new List<Tracker>((int)trackedBodies);

        // divide 'floatData' into segments of length 'elementsPerTracker', which are used to create 'Tracker' objects
        for (int startIndex = 0; startIndex < floatData.Length; startIndex += elementsPerTracker)
        {
            var segment = new ArraySegment<float>(floatData, startIndex, elementsPerTracker);
            trackers.Add(TrackerDataToTracker(segment));
        }

        return trackers;
    }

    
    private static Tracker TrackerDataToTracker(ArraySegment<float> data)
    {
        // check for clean data.Array access
        Debug.Assert(data.Array != null, "empty ArraySegment should never occur");

        // assume 17 values in data

        // Each data batch for a tracked body has a total of 17 values, where
        // 0 = Id, starting with 0
        // 1 = Quality -> ignored, see DTrack manual
        // 2, 3, 4 = Position
        // 5, 6, 7 = orientation angles
        // 8, 9, 10, 11, 12, 13, 14, 15, 16 = rotation matrix of the bodies rotation

        Matrix4x4 rotMatrix = Matrix4x4.identity;
        
        rotMatrix.SetColumn(0, new Vector4(
            data.Array[data.Offset + 8],
            data.Array[data.Offset + 9],
            data.Array[data.Offset + 10],
            0));
        rotMatrix.SetColumn(1, new Vector4(
            data.Array[data.Offset + 11],
            data.Array[data.Offset + 12],
            data.Array[data.Offset + 13],
            0));
        rotMatrix.SetColumn(2, new Vector4(
            data.Array[data.Offset + 14],
            data.Array[data.Offset + 15],
            data.Array[data.Offset + 16],
            0));

        return new Tracker
        {
            Id = (int)data.Array[data.Offset + 0],
            Position = new Vector3(
                data.Array[data.Offset + 2],
                data.Array[data.Offset + 3],
                data.Array[data.Offset + 4]),
            Rotation = rotMatrix.rotation,
            RotationMatrix = rotMatrix
        };
    }
}