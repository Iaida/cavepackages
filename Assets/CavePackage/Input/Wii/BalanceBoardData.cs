﻿using UnityEngine;

[System.Serializable]
public struct BalanceBoardData
{
    [Header("Weights in kg")]
    public float TopLeft;
    public float TopRight;
    public float BottomLeft;
    public float BottomRight;
    public float Total;

    public BalanceBoardData(float topLeft, float topRight, float bottomLeft, float bottomRight, float total)
    {
        TopLeft = topLeft;
        TopRight = topRight;
        BottomLeft = bottomLeft;
        BottomRight = bottomRight;
        Total = total;
    }


    public static BalanceBoardData operator -(BalanceBoardData a, BalanceBoardData b)
    {
        return new BalanceBoardData(
            a.TopLeft - b.TopLeft,
            a.TopRight - b.TopRight,
            a.BottomLeft - b.BottomLeft,
            a.BottomRight - b.BottomRight,
            a.Total - b.Total);
    }

    public static BalanceBoardData operator +(BalanceBoardData a, BalanceBoardData b)
    {
        return new BalanceBoardData(
            a.TopLeft + b.TopLeft,
            a.TopRight + b.TopRight,
            a.BottomLeft + b.BottomLeft,
            a.BottomRight + b.BottomRight,
            a.Total + b.Total);
    }

    public static BalanceBoardData operator /(BalanceBoardData bbd, int div)
    {
        return new BalanceBoardData(
            bbd.TopLeft / div,
            bbd.TopRight / div,
            bbd.BottomLeft / div,
            bbd.BottomRight / div,
            bbd.Total / div);
    }
}