﻿using System;
using UnityEngine;


[Serializable]
public struct BalanceBoardDistribution
{
    [Header("Percentages of total weight distribution in the 4 quadrants")]
    public float TopLeft;
    public float TopRight;
    public float BottomLeft;
    public float BottomRight;

    public BalanceBoardDistribution(BalanceBoardData balanceBoardData)
    {
        TopLeft = balanceBoardData.TopLeft / balanceBoardData.Total;
        TopRight = balanceBoardData.TopRight / balanceBoardData.Total;
        BottomLeft = balanceBoardData.BottomLeft / balanceBoardData.Total;
        BottomRight = balanceBoardData.BottomRight / balanceBoardData.Total;
    }

    private BalanceBoardDistribution(float topLeft, float topRight, float bottomLeft, float bottomRight)
    {
        TopLeft = topLeft;
        TopRight = topRight;
        BottomLeft = bottomLeft;
        BottomRight = bottomRight;
    }

    // default values are perfectly balanced, avoids division by zero
    public static BalanceBoardDistribution DefaultInstance()
    {
        return new BalanceBoardDistribution(0.25f, 0.25f, 0.25f, 0.25f);
    }
}