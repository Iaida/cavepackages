﻿using System.Globalization;
using UnityEngine;


public class BalanceBoardInput : MonoBehaviour
{
    public BalanceBoardData CalibrationBalanceBoardData;
    public BalanceBoardData BalanceBoardData;
    public BalanceBoardDistribution BalanceBoardDistribution;
    public Vector2 CenterOfMass;

    [Header("Settings")]
    public CaveSettings CaveSettings;
    public bool CalibrateOnStart = true;
    public float MinimumWeight = 10.0f;
    public bool ShowDebug;

    private WiiServerConnection _balanceBoardWiiServerConnection;

    public bool MeasureValidHz;
    // https://reverseengineering.stackexchange.com/questions/3791/wii-balance-board-sampling-frequency
    // only ~14 Hz with 'ReceiveBufferSize' of 64
    // ~47 Hz with 'ReceiveBufferSize' of 128
    // only ~24 Hz with 'ReceiveBufferSize' of 256
    public int Hz;
    private int _hzCounter;

    // affects the percentage of valid lines read from socket
    private const int ReceiveBufferSize = 128;

    // https://gamedev.stackexchange.com/questions/116009/in-unity-how-do-i-correctly-implement-the-singleton-pattern
    public static BalanceBoardInput Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        else
            Instance = this;
    }

    private void Start()
    {
        try
        {
            _balanceBoardWiiServerConnection = new WiiServerConnection(CaveSettings.WiiServerIp, 
                CaveSettings.BalanceBoardIdentifier, CaveSettings.WiiServerPort, ReceiveBufferSize);

            float? batteryLevel = _balanceBoardWiiServerConnection.GetBatteryLevel();
            if (!batteryLevel.HasValue)
            {
                _balanceBoardWiiServerConnection.Quit();
                throw new System.Exception("error occurred when requesting battery level");
            }

            Debug.Log("BalanceBoard battery at " + batteryLevel * 100.0f + "%");

            _balanceBoardWiiServerConnection.StartStream();

            if (CalibrateOnStart)
                CalibrationBalanceBoardData = CalcCalibration(5);
        }
        catch (System.Exception e)
        {
            Debug.LogError(e.Message + ", disabling " + GetType().Name);
            enabled = false;
        }

        if (MeasureValidHz)
            InvokeRepeating("UpdateHz", 0, 1);
    }

    private void Update()
    {
        BalanceBoardData? optionalBalanceBoardData = GetCurrentBalanceBoardData();
        if (optionalBalanceBoardData.HasValue)
        {
            BalanceBoardData = optionalBalanceBoardData.Value;
            if (CalibrateOnStart)
                ApplyCalibration();

            // only assign real values if 'MinimumWeight' exceeded
            BalanceBoardDistribution = (BalanceBoardData.Total > MinimumWeight)
                ? new BalanceBoardDistribution(BalanceBoardData)
                : BalanceBoardDistribution.DefaultInstance();
            CenterOfMass = GetCenterOfMass(BalanceBoardDistribution);

            _hzCounter++;
        }
        else
        {
            //_hzCounter++;
        }
    }

    private void OnDestroy()
    {
        if (enabled)
        {
            _balanceBoardWiiServerConnection.StopStream();
            _balanceBoardWiiServerConnection.Quit();
        }
    }

    
    private BalanceBoardData? GetCurrentBalanceBoardData()
    {
        BalanceBoardData? result = null;
        foreach (string dataString in _balanceBoardWiiServerConnection.ReadSocket())
        {
            BalanceBoardData? parseResult = TryParsingBalanceBoardData(dataString);
            if (parseResult.HasValue)
                result = parseResult;
        }
        return result;
    }


    private BalanceBoardData? TryParsingBalanceBoardData(string dataString)
    {
        if (string.IsNullOrEmpty(dataString) || !_balanceBoardWiiServerConnection.IsStreamString(dataString))
            return null;


        // STREAM:< identifier >;< top - left >,< top - right >,< bottom - left >,< bottom - right >,< total >

        // discard 'STREAM:< identifier >;'
        dataString = dataString.Substring(dataString.IndexOf(';') + 1);

        // remaining: '< top - left >,< top - right >,< bottom - left >,< bottom - right >,< total >'
        string[] splitResultWeights = StringUtilities.Split(dataString, ',');

        if (splitResultWeights.Length != 5)
            return null;

        // avoids error when parsing float with just "-" string at start of program
        float[] weights = new float[5];
        for (int i = 0; i < 5; i++)
        {
            if (!float.TryParse(splitResultWeights[i], NumberStyles.Any, CultureInfo.InvariantCulture, out weights[i]))
                return null;
        }

        return new BalanceBoardData(weights[0], weights[1], weights[2], weights[3], weights[4]);
    }

    private void ApplyCalibration()
    {
        BalanceBoardData = BalanceBoardData - CalibrationBalanceBoardData;
    }


    private static Vector2 GetCenterOfMass(BalanceBoardDistribution distribution)
    {
        return new Vector2(
            (distribution.TopRight + distribution.BottomRight) - (distribution.TopLeft + distribution.BottomLeft),
            (distribution.TopRight + distribution.TopLeft) - (distribution.BottomRight + distribution.BottomLeft));
    }


    private void OnGUI()
    {
        if (!ShowDebug)
            return;

        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();

        Rect rect = new Rect(0, 0, w, h * 0.02f);
        style.alignment = TextAnchor.UpperRight;
        style.fontSize = h * 2 / 100;
        style.normal.textColor = new Color(1.0f, 1.0f, 0.0f, 1.0f);

        string text = string.Format("{3:0} Hz \n {0:0.0} kg \n {1:0.00}, {2:0.00}", BalanceBoardData.Total, CenterOfMass.x, CenterOfMass.y, Hz);

        GUI.Label(rect, text, style);
    }


    // average over multiple iterations
    private BalanceBoardData CalcCalibration(int numToAverage)
    {
        BalanceBoardData accumulator = new BalanceBoardData(0, 0, 0, 0, 0);

        // try to get data 'numToAverage' times
        for (int i = 0; i < numToAverage; i++)
        {
            BalanceBoardData? current = null;
            while (!current.HasValue)
            {
                current = GetCurrentBalanceBoardData();
            }

            accumulator += current.Value;
        }

        return accumulator / numToAverage;
    }

    private void UpdateHz()
    {
        Hz = _hzCounter;
        _hzCounter = 0;
    }
}