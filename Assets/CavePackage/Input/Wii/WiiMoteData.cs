﻿using System;
using UnityEngine;

[Serializable]
public struct WiimoteData
{
    public enum Button
    {
        A,
        B,
        One,
        Two,
        Home,
        Left,
        Right,
        Down,
        Up,
        Plus,
        Minus
    }

    public static readonly int NumberButtons = Enum.GetNames(typeof(Button)).Length;


    // true -> pressed down, false -> not pressed
    public bool[] ButtonStates;
    public Vector3 Orientation;


    public WiimoteData(bool[] buttonStates, Vector3 orientation)
    {
        ButtonStates = buttonStates;
        Orientation = orientation;
    }

    public static WiimoteData DefaultInstance()
    {
        return new WiimoteData()
        {
            Orientation = Vector3.zero,
            ButtonStates = new bool[NumberButtons]
        };
    }

    public bool ButtonDown(Button button)
    {
        return ButtonStates[(int)button];
    }

    public Vector3 GetOrientation()
    {
        return Orientation;
    }
}