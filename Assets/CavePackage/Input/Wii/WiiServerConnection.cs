﻿using System;
using System.IO;
using System.Net.Sockets;

public struct WiiServerConnection
{
    private readonly NetworkStream _stream;
    private readonly StreamWriter _writer;
    private readonly string _identifier;
    private readonly byte[] _buffer;

    public WiiServerConnection(string ip, string deviceIdentifier, int port, int receiveBufferSize)
    {
        // local variable is sufficient
        TcpClient client = new TcpClient()
        {
            // default buffer size differs between windows versions which can potentially lead to buffer overflow on startup
            ReceiveBufferSize = receiveBufferSize
        };

        IAsyncResult result = client.BeginConnect(ip, port, null, null);
        if (!result.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(1)))
            throw new Exception("Failed to connect to ip " + ip + " with port " + port);
        client.EndConnect(result);

        _stream = client.GetStream();
        _writer = new StreamWriter(_stream);

        _identifier = deviceIdentifier;

        _buffer = new byte[receiveBufferSize];
    }

    // returns can contain multiple lines
    public string[] ReadSocket()
    {
        if (!_stream.DataAvailable)            
            return new string[0];

        int numBytesRead = _stream.Read(_buffer, 0, _buffer.Length);
        string bufferAsString = System.Text.Encoding.UTF8.GetString(_buffer, 0, numBytesRead);

        return StringUtilities.Split(bufferAsString, '\n');
    }

    private void WriteSocket(string message)
    {
        _writer.Write(message + "\r\n");
        _writer.Flush();
    }

    public void StartStream()
    {
        WriteSocket("STREAM " + _identifier);
    }

    public void StopStream()
    {
        WriteSocket("STREAM " + _identifier + " stop");
    }

    public void Quit()
    {
        WriteSocket("QUIT");
    }

    public bool IsStreamString(string input)
    {
        return input.StartsWith("STREAM:" + _identifier);
    }

    public void SetRumble(bool active)
    {
        WriteSocket("RUMBLE " + _identifier + " " + (active ? "on" : "off"));
    }

    public void SetLed(string led, bool active)
    {
        WriteSocket("SET " + _identifier + " " + led + " " + (active ? "on" : "off"));
    }

    // should be used before streaming is activated
    // returns value between 0 and 1 or null if failed
    public float? GetBatteryLevel()
    {
        WriteSocket("BATTERY " + _identifier);
        
        while (true)
        {
            foreach (string response in ReadSocket())
            {
                if (string.IsNullOrEmpty(response))
                    continue;

                if (response.StartsWith("ERROR"))
                    return null;

                // response syntax: BATTERY:<identifier>;<0.0-1.0>
                if (response.StartsWith("BATTERY"))
                {
                    string levelString = response.Substring(response.IndexOf(';') + 1);
                    return StringUtilities.StringToFloat(levelString);
                }
            }
            
        }
    }
}