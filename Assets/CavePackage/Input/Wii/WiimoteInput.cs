﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class WiimoteInput : MonoBehaviour
{
    public WiimoteData WiimoteData;
    public CaveSettings CaveSettings;

    private WiimoteData _previousWiimoteData;
    private WiiServerConnection _wiimoteWiiServerConnection;

    public bool MeasureValidHz;

    // only ~15 Hz with 'ReceiveBufferSize' of 256
    // ~44 Hz with 'ReceiveBufferSize' of 512
    // only ~24 Hz with 'ReceiveBufferSize' of 1024
    public int Hz; 
    private int _hzCounter;

    // affects the percentage of valid lines read from socket
    private const int ReceiveBufferSize = 512;

    private static readonly Dictionary<string, WiimoteData.Button> StringButtonDict = new Dictionary<string, WiimoteData.Button>()
    {
        {"a", WiimoteData.Button.A},
        {"b", WiimoteData.Button.B},
        {"one", WiimoteData.Button.One},
        {"two", WiimoteData.Button.Two},
        {"home", WiimoteData.Button.Home},
        {"left", WiimoteData.Button.Left},
        {"right", WiimoteData.Button.Right},
        {"down", WiimoteData.Button.Down},
        {"up", WiimoteData.Button.Up},
        {"plus", WiimoteData.Button.Plus},
        {"minus", WiimoteData.Button.Minus}
    };

    // DO NOT change enum names
    // named according to WiiServer procotol, used with Enum.ToString() in 'SetLed'
    public enum Led
    {
        led_1,
        led_2,
        led_3,
        led_4,
        led_all
    }


    // https://gamedev.stackexchange.com/questions/116009/in-unity-how-do-i-correctly-implement-the-singleton-pattern
    public static WiimoteInput Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        else
            Instance = this;
    }

    private void Start()
    {
        WiimoteData = WiimoteData.DefaultInstance();
        _previousWiimoteData = WiimoteData.DefaultInstance();

        try
        {
            _wiimoteWiiServerConnection = new WiiServerConnection(CaveSettings.WiiServerIp, 
                CaveSettings.WiimoteIdentifier, CaveSettings.WiiServerPort, ReceiveBufferSize);

            float? batteryLevel = _wiimoteWiiServerConnection.GetBatteryLevel();
            if (!batteryLevel.HasValue)
            {
                _wiimoteWiiServerConnection.Quit();
                throw new Exception("error occurred when requesting battery level");
            }

            Debug.Log("Wiimote battery at " + batteryLevel * 100.0f + "%");

            _wiimoteWiiServerConnection.StartStream();
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message + ", disabling " + GetType().Name);
            enabled = false;
        }


        if (MeasureValidHz)
            InvokeRepeating("UpdateHz", 0, 1);
    }

    private void Update()
    {
        // even if there is no new data, copy last valid data to '_previousWiimoteData' 
        // to avoid errors when detecting button presses and releases
        _previousWiimoteData = WiimoteData;

        WiimoteData? optionalWiimoteData = GetCurrentWiimoteData();
        if (optionalWiimoteData.HasValue)
        {
            _hzCounter++;
            WiimoteData = optionalWiimoteData.Value;
        }
    }

    
    private void OnDestroy()
    {
        if (enabled)
        {
            _wiimoteWiiServerConnection.StopStream();
            // make sure rumble and LEDs are turned off when exiting
            StopRumble();
            TurnOffAllLeds();
            _wiimoteWiiServerConnection.Quit();
        }
    }


    // button actions lasting only for a single frame are discarded, no event usage 
    private WiimoteData? GetCurrentWiimoteData()
    {
        WiimoteData? result = null;
        foreach (string dataString in _wiimoteWiiServerConnection.ReadSocket())
        {
            WiimoteData? parseResult = TryParsingWiimoteData(dataString);
            if (parseResult.HasValue)
                result = parseResult;
        }
        return result;
    }

    private WiimoteData? TryParsingWiimoteData(string dataString)
    {

        if (string.IsNullOrEmpty(dataString) || !_wiimoteWiiServerConnection.IsStreamString(dataString))
        {
            //Debug.Log(currentString);
            return null;
        }
            

        // STREAM:<identifier>;ROT:<roll>,<pitch>,<yaw>;(BTN:<btn>,<state>[;|])*
        string[] splitResult = StringUtilities.Split(dataString, ';');

        const int expectedSplitResultLength = 13;

        if (splitResult.Length != expectedSplitResultLength)
        {
            //Debug.Log(currentString);
            return null;
        }
            

        Vector3 orientation = ParseRotString(splitResult[1]);

        var buttons = new bool[WiimoteData.NumberButtons];
        // skip "STREAM:<identifier>;ROT:<roll>,<pitch>,<yaw>;" by starting array index at 2
        for (int i = 2; i < splitResult.Length; i++)
        {
            // splitResult[i] is "BTN:<btn>,<state>"
            KeyValuePair<WiimoteData.Button, bool>? optionalPair = ParseBtnString(splitResult[i]);
            if (optionalPair.HasValue)
                buttons[(int)optionalPair.Value.Key] = optionalPair.Value.Value;
        }

        return new WiimoteData(buttons, orientation);
    }


    private static Vector3 ParseRotString(string rotString)
    {
        // 'rotString' syntax is: ROT:<roll>,<pitch>,<yaw>
        string[] splitResult = StringUtilities.Split(rotString.Substring(4), ',');

        return new Vector3(
            StringUtilities.StringToFloat(splitResult[0]),
            StringUtilities.StringToFloat(splitResult[1]),
            StringUtilities.StringToFloat(splitResult[2]));
    }


    private static KeyValuePair<WiimoteData.Button, bool>? ParseBtnString(string buttonStateString)
    {
        // 'buttonStateString' syntax is: BTN:<btn>,<state>
        // discard "BTN:btn_"
        const int startIndex = 8;

        if (buttonStateString.Length < startIndex)
            return null;

        buttonStateString = buttonStateString.Substring(startIndex);

        string[] splitResult = StringUtilities.Split(buttonStateString, ',');

        if (splitResult.Length != 2)
            return null;

        // splitResult[0] should be a key in 'stringButtonDict'
        WiimoteData.Button button = StringButtonDict[splitResult[0]];
        // splitResult[1] should be "down" or "up"
        return new KeyValuePair<WiimoteData.Button, bool>(button, (splitResult[1] == "down"));
    }

    public Vector3 GetOrientation()
    {
        return WiimoteData.GetOrientation();
    }

    public bool ButtonDown(WiimoteData.Button button)
    {
        return WiimoteData.ButtonDown(button);
    }

    public bool ButtonPressed(WiimoteData.Button button)
    {
        return WiimoteData.ButtonDown(button) && !_previousWiimoteData.ButtonDown(button);
    }

    public bool ButtonReleased(WiimoteData.Button button)
    {
        return !WiimoteData.ButtonDown(button) && _previousWiimoteData.ButtonDown(button);
    }

    

    public void RumbleForSeconds(float seconds)
    {
        StartRumble();
        Invoke("StopRumble", seconds);
    }

    public void StopRumble()
    {
        _wiimoteWiiServerConnection.SetRumble(false);
    }

    public void StartRumble()
    {
        _wiimoteWiiServerConnection.SetRumble(true);
    }

    public void SetLeds(Led led, bool active)
    {
        _wiimoteWiiServerConnection.SetLed(led.ToString(), active);
    }

    public void TurnOffAllLeds()
    {
        SetLeds(Led.led_all, false);
    }

    [ContextMenu("RumbleTest")]
    private void RumbleTest()
    {
        RumbleForSeconds(1.0f);
    }

    [ContextMenu("LedOnTest")]
    private void LedOnTest()
    {
        SetLeds(Led.led_all, true);
    }

    [ContextMenu("LedOffTest")]
    private void LedOffTest()
    {
        TurnOffAllLeds();
    }

    private void UpdateHz()
    {
        Hz = _hzCounter;
        _hzCounter = 0;
    }



}