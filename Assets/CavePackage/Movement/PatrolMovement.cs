﻿using UnityEngine;

// script for patrolling movement between 'StartPoint' and 'EndPoint'
// one way takes 'durationSec' seconds
public class PatrolMovement : MonoBehaviour
{
    public Vector3 StartPoint = new Vector3(1000, 1500, 1000);
    public Vector3 EndPoint = new Vector3(-1000, 1500, 1000);

    public float DurationSec = 2.0f;
    // ranges from 0 to 2
    public float T;

    private void Start()
    {
        transform.position = StartPoint;
    }

    private void Update()
    {
        T += Time.deltaTime / DurationSec;
        if (T > 2.0f)
            T = 0.0f;

        transform.position = MathUtilities.LerpPingPong(StartPoint, EndPoint, T);
    }
}