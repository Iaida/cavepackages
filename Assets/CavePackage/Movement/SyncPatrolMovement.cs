﻿#pragma warning disable 0618 // UNet Deprecation warning disabled

using UnityEngine;
using UnityEngine.Networking;


[RequireComponent(typeof(PatrolMovement))]
[NetworkSettings(channel = 0, sendInterval = 0)]
public class SyncPatrolMovement : NetworkBehaviour
{
    private PatrolMovement _patrolMovement;

    private void Awake()
    {
        _patrolMovement = GetComponent<PatrolMovement>();
    }

    [ServerCallback]
    private void Start()
    {
        // sync only every 'DurationSec' seconds
        InvokeRepeating("Synchronize", 0, _patrolMovement.DurationSec);
    }

    [ClientRpc]
    private void RpcSyncPatrolMovement(float t)
    {
        _patrolMovement.T = t;
    }


    private void Synchronize()
    {
        RpcSyncPatrolMovement(_patrolMovement.T);
    }
}
