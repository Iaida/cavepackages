﻿#pragma warning disable 0618 // UNet Deprecation warning disabled

using UnityEngine;
using UnityEngine.Networking;

// naive implementation without any interpolation which results in visible stuttering
[NetworkSettings(channel = 0, sendInterval = 0)]
public class SyncTransform : NetworkBehaviour
{
    [ServerCallback]
    private void LateUpdate ()
    {
        if (transform.hasChanged)
        {
            RpcSyncTransform(transform.localPosition, transform.localRotation, transform.localScale);
            transform.hasChanged = false;
        }
	}

    [ClientRpc]
    private void RpcSyncTransform(Vector3 localPosition, Quaternion localRotation, Vector3 localScale)
    {
        transform.localPosition = localPosition;
        transform.localRotation = localRotation;
        transform.localScale = localScale;
    }
}
