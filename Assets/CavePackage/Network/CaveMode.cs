﻿using UnityEngine;

public class CaveMode : MonoBehaviour
{
    public enum Mode
    {
        Front,
        Bottom,
        Right,
        Left,
        Client,
        Server
    }

    // 'CurrentMode' can be set in inspector for debugging purposes <without> command line parameters under windows
    // Useful when debugging networking functions on a single local machine

    // Standalone Build:    'CurrentMode': 'Front'
    // Editor:              'CurrentMode': 'Server'
    // Run a front cave client in standalone while running the server in Unity.

    // Standalone Build:    'CurrentMode': 'Server'
    // Editor:              'CurrentMode': 'Front'
    // Run a server in standalone while running the front cave client in Unity.

    public Mode CurrentMode = Mode.Server;

    // https://gamedev.stackexchange.com/questions/116009/in-unity-how-do-i-correctly-implement-the-singleton-pattern
    public static CaveMode Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        else
            Instance = this;
    }

    public bool ParseMode(string modeString)
    {
        switch (modeString)
        {
            case "-left":
                CurrentMode = Mode.Left;
                break;
            case "-right":
                CurrentMode = Mode.Right;
                break;
            case "-bottom":
                CurrentMode = Mode.Bottom;
                break;
            case "-front":
                CurrentMode = Mode.Front;
                break;
            case "-client":
                CurrentMode = Mode.Client;
                break;
            case "-server":
                CurrentMode = Mode.Server;
                break;
            default:
                return false;
        }

        return true;
    }

    public bool IsInCaveMode()
    {
        switch (CurrentMode)
        {
            case Mode.Front:
            case Mode.Bottom:
            case Mode.Right:
            case Mode.Left:
                return true;
            default:
                return false;
        }
    }

    public bool IsInMode(Mode mode)
    {
        return CurrentMode == mode;
    }

    // use in rpc sound calls of clients
    public bool ShouldPlaySound(bool buttkickerSound = false)
    {
        return CurrentMode != Mode.Bottom || buttkickerSound;
    }
}