﻿#pragma warning disable 0618 // UNet Deprecation warning disabled

using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class CaveNetworkManager : NetworkManager
{
    [Header("Cave-specific Settings")]
    [Space(10)]
    public CaveSettings CaveSettings;
    public bool StartAutomatically = true;
    public bool ReconnectClientsAutomatically = true;
    public bool DebugSingleLocalCaveClient;
    public bool DebugOnlyServer;
    public uint NumberOfAdditionalClients;
    

    private const uint MaximumNumberOfCaveClients = 4;
    private uint _numConnectedCaveClients;
    private uint _numConnectedAdditionalClients;


    private void Start()
    {
        networkAddress = CaveSettings.ServerIp;

        if (!StartAutomatically)
            return;

        if (CaveMode.Instance.CurrentMode == CaveMode.Mode.Server)
        {
            // This uses the networkPort property as the listen port.
            StartServer();

            if (DebugOnlyServer)
            {
                Debug.Log("DebugOnlyServer");
                // simulates clients connecting, delayed to allow other start functions to finish
                Invoke("AllClientsConnected", 3.0f);
            }
        }
        else
        {
            // This starts a network client. It uses the networkAddress and networkPort properties as the address to connect to.
            StartClient();
        }
    }

    // Called on clients when disconnected from a server.
    public override void OnClientDisconnect(NetworkConnection conn)
    {
        base.OnClientDisconnect(conn);
        if (ReconnectClientsAutomatically)
        {
            Debug.LogError("client disconnect occurred, restarting client... ");
            StartClient();
        }

    }

    // Called on the server when a client is ready.
    // executed later than 'OnServerConnect'
    public override void OnServerReady(NetworkConnection conn)
    {
        base.OnServerReady(conn);

        Debug.Log("New client ready with ip " + conn.FormattedIp());

        if (DebugSingleLocalCaveClient)
        {
            Debug.Log("DebugSingleLocalCaveClient");
            AllClientsConnected();
        }
        else
        {
            if (IsCaveConnection(conn))
                _numConnectedCaveClients++;
            else
                _numConnectedAdditionalClients++;

            if (_numConnectedCaveClients == MaximumNumberOfCaveClients && 
                _numConnectedAdditionalClients == NumberOfAdditionalClients)
            {
                Debug.Log("numMaxCaveClients and numConnectedAdditionalClients reached");
                AllClientsConnected();
            }
        }
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        base.OnServerDisconnect(conn);
        Debug.LogError("A client disconnected");

        if (IsCaveConnection(conn))
            _numConnectedCaveClients--;
        else
            _numConnectedAdditionalClients--;
    }

    private void AllClientsConnected()
    {
        Debug.Log("Calling 'OnAllClientsConnected' on NetworkBehaviourCave components...");

        // https://answers.unity.com/questions/863509/how-can-i-find-all-objects-that-have-a-script-that.html
        foreach (IAllClientsConnected i in FindObjectsOfType<NetworkBehaviour>().OfType<IAllClientsConnected>())
        {
            i.OnAllClientsConnected();
        }
    }

    private bool IsCaveConnection(NetworkConnection conn)
    {
        string ip = conn.FormattedIp();

        return ip == CaveSettings.CaveFrontIp ||
               ip == CaveSettings.CaveDownIp ||
               ip == CaveSettings.CaveRightIp ||
               ip == CaveSettings.CaveLeftIp;
    }


    public IEnumerable<NetworkConnection> GetAdditionalConnections()
    {
        return NetworkServer.connections.Where(c => !IsCaveConnection(c));
    }



    [ContextMenu("PrintRoundTripTimes")]
    private void PrintRoundTripTimes()
    {
        foreach (NetworkConnection c in NetworkServer.connections)
        {
            // ignore disconnected connections
            if (c == null)
                continue;

            byte error;
            int rtt = NetworkTransport.GetCurrentRTT(c.hostId, c.connectionId, out error);
            NetworkError networkError = (NetworkError)error;
            string errorString = networkError != NetworkError.Ok ? " Error: " + networkError : "";
            Debug.Log("RTT for " + c.FormattedIp() + ": " + rtt + errorString);
        }
    }

    [ContextMenu("PrintChannels")]
    private void PrintChannels()
    {
        // https://docs.unity3d.com/ScriptReference/Networking.NetworkSettingsAttribute-channel.html
        // https://docs.unity3d.com/ScriptReference/Networking.QosType.html

        // https://docs.unity3d.com/ScriptReference/Networking.Channels.html
        Debug.Log("Default Channels:");
        Debug.Log("Default Reliable: " + Channels.DefaultReliable);
        Debug.Log("Default Unreliable: " + Channels.DefaultUnreliable);

        // https://docs.unity3d.com/ScriptReference/Networking.NetworkManager-channels.html
        Debug.Log("Number of configured Channels: " + channels.Count);
        for (int i = 0; i < channels.Count; i++)
        {
            Debug.Log("Channel with index " + i + ": " + channels[i]);
        }
    }

    public NetworkConnection ClientConnectionFromIp(string ip)
    {
        // NetworkServer.connections can contain null references, checked first
        return NetworkServer.connections.FirstOrDefault(c => c != null && c.FormattedIp() == ip);
    }


}