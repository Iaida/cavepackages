﻿#pragma warning disable 0618 // UNet Deprecation warning disabled

using UnityEngine;
using UnityEngine.Networking;

[NetworkSettings(channel = 0, sendInterval = 0)]
public class CommandUsage : NetworkBehaviour
{
    // trivial sync via 'SyncVar' attribute: 
    // https://docs.unity3d.com/ScriptReference/Networking.SyncVarAttribute.html
    [SyncVar]
    public int CounterSync;

    // non-trivial synchronization via rpc call:
    // https://docs.unity3d.com/ScriptReference/Networking.ClientRpcAttribute.html
    public int CounterRpc;

    private GUIStyle _style;
    private Rect _rect;


    private void Start()
    {
        _style = new GUIStyle
        {
            alignment = TextAnchor.MiddleCenter,
            fontSize = Screen.height * 2 / 100,
            normal = {textColor = Color.yellow}
        };
        _rect = new Rect(0, 0, Screen.width, Screen.height * 0.05f);
    }


    private void OnGUI()
    {
        string text = string.Format("CounterSync: {0:0} \n {1:0}", CounterSync, CounterRpc);

        GUI.Label(_rect, text, _style);
    }

    [ClientCallback]
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            CmdIncrementCounterSync();
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            CmdIncrementCounterRpc();
        }
    }


    [Command]
    private void CmdIncrementCounterSync()
    {
        IncrementCounterSync();
    }

    [Command]
    private void CmdIncrementCounterRpc()
    {
        // can execute function locally and on clients via rpc
        // OR execute function locally and send the result via rpc call parameters
        IncrementCounterRpc();
        RpcIncrementCounterRpc();
    }


    private void IncrementCounterSync()
    {
        Debug.Log("'IncrementCounterSync' called");
        CounterSync++;
    }

    private void IncrementCounterRpc()
    {
        Debug.Log("'IncrementCounterRpc' called");
        CounterRpc++;
    }

    [ClientRpc]
    private void RpcIncrementCounterRpc()
    {
        IncrementCounterRpc();
    }
}