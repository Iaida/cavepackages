﻿// used by 'CaveNetworkManager' in 'AllClientsConnected' function

internal interface IAllClientsConnected
{
    void OnAllClientsConnected();
}