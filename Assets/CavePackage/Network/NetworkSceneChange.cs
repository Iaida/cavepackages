﻿#pragma warning disable 0618 // UNet Deprecation warning disabled

using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

// only a workaround script, does not use the recommended NetworkManager's ServerChangeScene function,
// which would need a additive scene structure with persistent objects

[NetworkSettings(channel = 0, sendInterval = 0)]
public class NetworkSceneChange : NetworkBehaviour
{
    public CaveNetworkManager CaveNetworkManager;

    // function to be used by other scripts after retrieving a reference to this script
    // for larger scenes, 'clientSceneLoadDelaySeconds' might need to be increased
    [ServerCallback]
    public void LoadSceneAll(string sceneName, float serverSceneLoadDelaySeconds = 1.0f, float clientSceneLoadDelaySeconds = 2.0f)
    {
        if (!Application.CanStreamedLevelBeLoaded(sceneName))
        {
            Debug.LogError(sceneName + " is not a valid scene name");
            return;
        }

        if (clientSceneLoadDelaySeconds <= serverSceneLoadDelaySeconds)
        {
            Debug.LogError("ClientSceneLoadDelaySeconds should be greater than ServerSceneLoadDelaySeconds");
            return;
        }

        // 'RpcLoadScene' is executed before 'ServerLoadScene' and
        // 'ServerLoadScene's function content is delayed to make sure the rpc call is received by clients.
        // The delay parameters should cause the server scene to be loaded before the client scenes.
        RpcLoadScene(clientSceneLoadDelaySeconds, sceneName);
        ServerLoadScene(serverSceneLoadDelaySeconds, sceneName);
    }

    // based on: https://docs.unity3d.com/ScriptReference/AsyncOperation-allowSceneActivation.html
    private IEnumerator LoadScene(float delaySec, string sceneName, Action finishedLoadingAction)
    {
        yield return new WaitForSeconds(delaySec);

        //Begin to load the Scene you specify
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneName);
        //Don't let the Scene activate until you allow it to
        asyncOperation.allowSceneActivation = false;
        Debug.Log("Pro :" + asyncOperation.progress);
        //When the load is still in progress, output the Text and progress bar
        while (!asyncOperation.isDone)
        {
            //Output the current progress
            Debug.Log("Loading progress: " + (asyncOperation.progress * 100) + "%");

            // Check if the load has finished
            if (asyncOperation.progress >= 0.9f)
            {
                //Change the Text to show the Scene is ready
                Debug.Log("Finished loading");
                finishedLoadingAction();
                asyncOperation.allowSceneActivation = true;
            }

            yield return null;
        }
    }

    [ClientRpc]
    private void RpcLoadScene(float delaySec, string sceneName)
    {
        StartCoroutine(LoadScene(delaySec, sceneName, () =>
        {
            // gets reset to default by loading a new scene
            CaveNetworkManager.ReconnectClientsAutomatically = false;
            CaveNetworkManager.StopClient();
        }));
    }

    [ServerCallback]
    private void ServerLoadScene(float delaySec, string sceneName)
    {
        StartCoroutine(LoadScene(delaySec, sceneName, 
            CaveNetworkManager.StopServer));
    }
}
