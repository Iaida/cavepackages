﻿#pragma warning disable 0618 // UNet Deprecation warning disabled

using UnityEngine.Networking;

[NetworkSettings(channel = 0, sendInterval = 0)]
public class PlayerName : NetworkBehaviour
{
    private NetworkIdentity _networkIdentity;

    private void Awake()
    {
        _networkIdentity = GetComponent<NetworkIdentity>();
    }

    private void Start()
    {
        NetworkConnection conn = isLocalPlayer ? 
            _networkIdentity.connectionToServer : 
            _networkIdentity.connectionToClient;

        name = "Player (" + conn.FormattedIp() + ")";
    }
}
