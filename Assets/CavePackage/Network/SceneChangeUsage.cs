﻿using UnityEngine;


public class SceneChangeUsage : MonoBehaviour
{
    public NetworkSceneChange NetworkSceneChange;

    public string TestSceneName;


    [ContextMenu("SceneChange")]
    private void SceneChange()
    {
        NetworkSceneChange.LoadSceneAll(TestSceneName);
    }

    // for debugging on windows standalone
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
            SceneChange();
    }
}
