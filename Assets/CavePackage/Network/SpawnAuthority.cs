﻿#pragma warning disable 0618 // UNet Deprecation warning disabled

using UnityEngine;
using UnityEngine.Networking;

[NetworkSettings(channel = 0, sendInterval = 0)]
public class SpawnAuthority : NetworkBehaviour
{
    public GameObject Prefab;

    public override void OnStartClient()
    {
        base.OnStartClient();
        // necessary for usage of NetworkServer.Spawn
        ClientScene.RegisterPrefab(Prefab);
        Debug.Log("Registered prefab " + Prefab);
    }

    private void Awake()
    {
        Prefab.CheckForComponentAndPrintError<NetworkIdentity>();
        if (!Prefab.GetComponent<NetworkIdentity>().localPlayerAuthority)
            Debug.LogError("Prefab's " + Prefab.name + "'NetworkIdentity.localPlayerAuthority' should be true");
    }

    [ClientCallback]
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Spawn();
        }
    }

    [ContextMenu("Spawn")]
    public void Spawn()
    {
        if (!CaveMode.Instance.IsInMode(CaveMode.Mode.Client))
        {
            Debug.LogError("Only additional clients should send command via 'PlayerAuthority'");
            return;
        }       

        CmdSpawn();
    }

    [Command]
    private void CmdSpawn()
    {
        GameObject go = Instantiate(Prefab, transform.position, Quaternion.identity);
        go.name += "(" + name + ")";
        NetworkServer.SpawnWithClientAuthority(go, connectionToClient);
    }
}
