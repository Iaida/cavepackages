﻿#pragma warning disable 0618 // UNet Deprecation warning disabled

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

// based on: 
// https://www.youtube.com/watch?v=9-zDOJllPZ8
// https://docs.unity3d.com/Manual/UNetCustomSpawning.html

[NetworkSettings(channel = 0, sendInterval = 0)]
public class ObjectPoolSync : NetworkBehaviour
{
    public GameObject PooledObjectPrefab;
    public int PooledAmount = 10;
    [Tooltip("Can exceed 'PooledAmount' number of objects")]
    public bool IncreasesSize = true;

    private NetworkHash128 _assetId;
    private List<GameObject> _pooledObjects;

    public override void OnStartClient()
    {
        base.OnStartClient();
        // necessary for usage of NetworkServer.Spawn
        ClientScene.RegisterPrefab(PooledObjectPrefab);
        Debug.Log("Registered prefab " + PooledObjectPrefab);
    }

    private void Start()
    {
        PooledObjectPrefab.CheckForComponentAndPrintError<NetworkIdentity>();

        _pooledObjects = new List<GameObject>(PooledAmount);
        for (int i = 0; i < PooledAmount; i++)
        {
            GameObject instance = Instantiate(PooledObjectPrefab);
            instance.SetActive(false);
            instance.transform.parent = transform;
            _pooledObjects.Add(instance);
        }

        _assetId = PooledObjectPrefab.GetComponent<NetworkIdentity>().assetId;
        ClientScene.RegisterSpawnHandler(_assetId, SpawnObject, UnSpawnObject);
    }

    public GameObject GetPooledObject(Vector3 position, Quaternion rotation = default(Quaternion))
    {
        // search for an inactive object in pool
        int index = _pooledObjects.FindIndex(go => !go.activeInHierarchy);
        if (index != -1)
        {
            GameObject pooledGo = _pooledObjects[index];
            pooledGo.transform.position = position;
            pooledGo.transform.rotation = rotation;
            pooledGo.SetActive(true);
            pooledGo.transform.parent = transform;
            return pooledGo;
        }
        // no inactive object left in pool
        if (!IncreasesSize)
            return null;

        // 'IncreasesSize' true, create a new object for '_pooledObjects' list
        GameObject newInstance = Instantiate(PooledObjectPrefab);
        _pooledObjects.Add(newInstance);
        newInstance.transform.position = position;
        newInstance.transform.rotation = rotation;
        newInstance.SetActive(true);
        newInstance.transform.parent = transform;
        return newInstance;
    }

    public void ReturnObjectToPool(GameObject spawnedObject)
    {
        spawnedObject.SetActive(false);
    }

    // follows signature of: https://docs.unity3d.com/ScriptReference/Networking.UnSpawnDelegate.html
    private void UnSpawnObject(GameObject spawnedObject)
    {
        ReturnObjectToPool(spawnedObject);
    }

    // follows signature of: https://docs.unity3d.com/ScriptReference/Networking.SpawnDelegate.html 
    private GameObject SpawnObject(Vector3 position, NetworkHash128 assetId)
    {
        return GetPooledObject(position);
    }

}
