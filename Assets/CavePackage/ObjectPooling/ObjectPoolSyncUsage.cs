﻿#pragma warning disable 0618 // UNet Deprecation warning disabled

using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

// demonstrates usage of 'ObjectPool' component
[RequireComponent(typeof(ObjectPoolSync))]
[NetworkSettings(channel = 0, sendInterval = 0)]
public class ObjectPoolSyncUsage : NetworkBehaviour
{
    public float SpawnRadius = 1000;
    public float LifeTime = 3.0f;

    private ObjectPoolSync _objectPoolSync;

    private void Awake ()
	{
	    _objectPoolSync = GetComponent<ObjectPoolSync>();
	}

    [ContextMenu("Spawn")]
    private void Spawn()
    {
        Vector3 spawnLocation = transform.position + RandomUtilities.PositionOnDisk(SpawnRadius, 0.0f);
        GameObject instance = _objectPoolSync.GetPooledObject(spawnLocation);
        // - changes to the 'instance' GameObject can be made here -
        NetworkServer.Spawn(instance);

        StartCoroutine(DestroyAfterSeconds(instance, LifeTime));
    }

    private IEnumerator DestroyAfterSeconds(GameObject go, float timer)
    {
        yield return new WaitForSeconds(timer);
        _objectPoolSync.ReturnObjectToPool(go);
        NetworkServer.UnSpawn(go);
    }

}
