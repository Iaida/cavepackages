﻿#pragma warning disable 0618 // UNet Deprecation warning disabled

using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(ObjectPoolSync))]
[NetworkSettings(channel = 0, sendInterval = 0)]
public class FlamePlacementSync : NetworkBehaviour, IAllClientsConnected
{
    public int NumberOfObjects = 100;
    public float SpawnRadius = 5;

    private ObjectPoolSync _objectPoolSync;

    private void Awake()
    {
        _objectPoolSync = GetComponent<ObjectPoolSync>();
        _objectPoolSync.PooledObjectPrefab.CheckForComponentAndPrintError<SyncParticleSystems>();
    }

    public void OnAllClientsConnected()
    {
        Spawn();
    }

    // called from synchronizeComponents after clients have connected in CaveNetworkManager
    // can also be called manually to spawn at later time
    [ContextMenu("Spawn")]
    [ServerCallback]
    private void Spawn()
    {
        for (int i = 0; i < NumberOfObjects; i++)
        {
            Vector3 spawnLocation = transform.position + RandomUtilities.PositionOnDisk(SpawnRadius, 0.0f);
            GameObject instance = _objectPoolSync.GetPooledObject(spawnLocation);
            // - changes to the 'instance' GameObject can be made here -
            NetworkServer.Spawn(instance);
            
            instance.GetComponent<SyncParticleSystems>().RestartAll();
        }
    }

    [ContextMenu("Unspawn")]
    public void UnSpawnChildren()
    {
        foreach (Transform childTransform in transform)
        {
            _objectPoolSync.ReturnObjectToPool(childTransform.gameObject);
            // https://docs.unity3d.com/ScriptReference/Networking.NetworkServer.UnSpawn.html
            NetworkServer.UnSpawn(childTransform.gameObject);
        }
    }
}