﻿using System.Collections.Generic;
using UnityEngine;

// utility script for displaying various variables of a ParticleSystem component in inspector for debugging purposes
[RequireComponent(typeof(ParticleSystem))]
public class ParticleSystemInfo : MonoBehaviour
{
    private ParticleSystem _ps;

    public float Time;
    public float Duration;
    public float MaxLifetime;
    public uint CurrentSeed;

    private void Start()
    {
        _ps = GetComponent<ParticleSystem>();
        Duration = _ps.main.duration;
        MaxLifetime = MaxLifeTimePossible(_ps.main.startLifetime);
    }


    private void Update()
    {
        Time = _ps.time;
        CurrentSeed = _ps.randomSeed;
    }


    private static float MaxLifeTimePossible(ParticleSystem.MinMaxCurve curve)
    {
        // https://docs.unity3d.com/ScriptReference/ParticleSystemCurveMode.html
        switch (curve.mode)
        {
            case ParticleSystemCurveMode.Constant:
                return curve.constant;
            case ParticleSystemCurveMode.TwoConstants:
                return curve.constantMax;
            case ParticleSystemCurveMode.Curve:
                return MaxCurveValue(curve.curve) * curve.curveMultiplier;
            case ParticleSystemCurveMode.TwoCurves:
                return Mathf.Max(MaxCurveValue(curve.curveMin), MaxCurveValue(curve.curveMax)) * curve.curveMultiplier;
            default:
                throw new System.ArgumentException("invalid mode");
        }
    }

    private static float MaxCurveValue(AnimationCurve curve)
    {
        // transform array of keyframes to array of values
        var keyframeList = new List<Keyframe>(curve.keys);
        float[] valueArray = keyframeList.ConvertAll(kf => kf.value).ToArray();
        // maximum of all values
        return Mathf.Max(valueArray);
    }
}