﻿#pragma warning disable 0618 // UNet Deprecation warning disabled

using System;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(ParticleSystem))]
[NetworkSettings(channel = 0, sendInterval = 0)]
public class SyncParticleSystems : NetworkBehaviour, IAllClientsConnected
{
    public bool WithChildren = true;
    public bool Prewarm;

    private ParticleSystem _ps;
    private ParticleSystem[] _childrenPs;

    private void Awake()
    {
        _ps = GetComponent<ParticleSystem>();
        _childrenPs = GetComponentsInChildren<ParticleSystem>();

        DoParticleSystemAction(ps => SetPrewarm(ps, Prewarm));
    }


    private static void SetPrewarm(ParticleSystem ps, bool prewarm)
    {
        // Particle system modules do not need to be reassigned back to the system; they are interfaces and not independent objects.
        ParticleSystem.MainModule main = ps.main;
        // https://docs.unity3d.com/ScriptReference/ParticleSystem.MainModule-prewarm.html
        main.prewarm = prewarm;
    }

    [ClientRpc]
    private void RpcRestart(uint seed)
    {
        Restart(seed);
    }

    [ServerCallback]
    public void RestartAll()
    {
        // use own custom seed function instead of unity's internal ParticleSystem seed
        uint seed = RandomUtilities.GenerateSeed();
        //uint seed = _ps.randomSeed;

        RpcRestart(seed);
        Restart(seed);
    }

    private void Restart(uint seed)
    {
        _ps.Stop(WithChildren, ParticleSystemStopBehavior.StopEmittingAndClear);

        DoParticleSystemAction(ps => ps.randomSeed = seed);

        _ps.Play(WithChildren);
    }



    // helper function for particle system tasks which are not supported by unity with a function which has a 'withChildren' parameter
    private void DoParticleSystemAction(Action<ParticleSystem> action)
    {
        action(_ps);
        if (WithChildren)
        {
            foreach (ParticleSystem cps in _childrenPs)
            {
                action(cps);
            }
        }
    }
    
    public void OnAllClientsConnected()
    {
        RestartAll();
    }
}