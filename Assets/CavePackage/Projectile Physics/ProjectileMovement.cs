﻿#pragma warning disable 0618 // UNet Deprecation warning disabled

using UnityEngine;
using UnityEngine.Networking;


[RequireComponent(typeof(Rigidbody))]
[NetworkSettings(channel = 0, sendInterval = 0)]
public class ProjectileMovement : NetworkBehaviour
{
    // https://docs.unity3d.com/ScriptReference/Networking.SyncVarAttribute.html
    // set after instantiating of prefab in 'ProjectileSpawner' script
    [SyncVar] public Vector3 StartVelocity;
    public ObjectPoolSync ObjectPool;

    private Rigidbody _rigidBody;

    private void Awake()
    {
        _rigidBody = GetComponent<Rigidbody>();
    }

    // state of the object and its SyncVars is guaranteed to be up-to-date
    public override void OnStartClient()
    {
        base.OnStartClient();
        ApplyVelocity();
    }

    [ServerCallback]
    private void Start()
    {
        ApplyVelocity();
    }

    private void Update()
    {
        transform.LookAt(transform.position + _rigidBody.velocity);
    }

    private void ApplyVelocity()
    {
        _rigidBody.velocity = StartVelocity;
    }
}