﻿#pragma warning disable 0618 // UNet Deprecation warning disabled

using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

[NetworkSettings(channel = 0, sendInterval = 0)]
public class ProjectileSpawner : NetworkBehaviour, IAllClientsConnected
{
    public ObjectPoolSync ObjectPool;

    public float StartDelay = 3.0f;
    public float SpawnFrequency = 0.5f;
    public float Speed = 10000.0f;

    // approximate player position in cave
    public Vector3 TargetPosition = new Vector3(0.0f, 1500.0f, 0.0f);

    public float MaxLifeTime = 10.0f;

    private void Awake()
    {
        ObjectPool.PooledObjectPrefab.CheckForComponentAndPrintError<ProjectileMovement>();
    }


    [ContextMenu("SpawnTest")]
    [ServerCallback]
    private void StartSpawning()
    {
        InvokeRepeating("Spawn", StartDelay, SpawnFrequency);
    }

    private void Spawn()
    {
        GameObject instance = ObjectPool.GetPooledObject(transform.position);
        instance.GetComponent<ProjectileMovement>().StartVelocity = (TargetPosition - instance.transform.position).normalized * Speed;
        NetworkServer.Spawn(instance);

        StartCoroutine(DestroyAfterSeconds(instance, MaxLifeTime));
    }

    private IEnumerator DestroyAfterSeconds(GameObject go, float timer)
    {
        yield return new WaitForSeconds(timer);
        ObjectPool.ReturnObjectToPool(go);
        NetworkServer.UnSpawn(go);
    }

    public void OnAllClientsConnected()
    {
        StartSpawning();
    }

}
