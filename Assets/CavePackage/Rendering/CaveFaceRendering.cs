﻿using UnityEngine;

// Renders the camera to cave face textures including the correct projection
// renders only a single 'eye' without any offset

public class CaveFaceRendering : MonoBehaviour
{
    public CaveSettings CaveSettings;

    // assigned in inspector via drag-and-drop of cave face
    public Renderer FrontRenderer;
    public Renderer BottomRenderer;
    public Renderer RightRenderer;
    public Renderer LeftRenderer;

    private RenderTexture _frontRenderTexture;
    private RenderTexture _bottomRenderTexture;
    private RenderTexture _rightRenderTexture;
    private RenderTexture _leftRenderTexture;

    private StereoRendering _stereoRendering;
    private Camera _debugCamera;

    private void Start()
    {
        _stereoRendering = StereoRendering.Instance;

        _debugCamera = _stereoRendering.GenerateCamera(StereoRendering.Eye.None, "DebugCamera");
        // disable camera to allow for manual rendering
        _debugCamera.enabled = false;

        _frontRenderTexture = GenerateEmptyRenderTexture();
        FrontRenderer.material.mainTexture = _frontRenderTexture;

        _bottomRenderTexture = GenerateEmptyRenderTexture();
        BottomRenderer.material.mainTexture = _bottomRenderTexture;

        _rightRenderTexture = GenerateEmptyRenderTexture();
        RightRenderer.material.mainTexture = _rightRenderTexture;

        _leftRenderTexture = GenerateEmptyRenderTexture();
        LeftRenderer.material.mainTexture = _leftRenderTexture;

    }

    private void LateUpdate()
    {
        RenderCaveFace(CaveMode.Mode.Front, _frontRenderTexture, FrontRenderer);
        RenderCaveFace(CaveMode.Mode.Bottom, _bottomRenderTexture, BottomRenderer);
        RenderCaveFace(CaveMode.Mode.Right, _rightRenderTexture, RightRenderer);
        RenderCaveFace(CaveMode.Mode.Left, _leftRenderTexture, LeftRenderer);
    }

    private void RenderCaveFace(CaveMode.Mode mode, RenderTexture renderTexture, Renderer faceRenderer)
    {
        // disable rendering component of the face object to allow for rendering of objects outside of the cave cube
        faceRenderer.enabled = false;

        CaveProjectionArea caveProjectionArea = CaveProjectionArea.CaveProjectionAreaFromMode(mode);
        _stereoRendering.UpdateCameraFrustum(_debugCamera, caveProjectionArea);

        _debugCamera.targetTexture = renderTexture;

        // manual clear necessary, else previous frame pixels remain in texture until overridden
        _debugCamera.targetTexture.Release();
        _debugCamera.Render();

        // reenable rendering component after rendering to the RenderTexture
        faceRenderer.enabled = true;
    }

    private RenderTexture GenerateEmptyRenderTexture()
    {
        RenderTexture emptyRenderTexture = new RenderTexture(CaveSettings.ScreenWidth, CaveSettings.ScreenHeight, 0, RenderTextureFormat.ARGB32);
        emptyRenderTexture.Create();
        return emptyRenderTexture;
    }
}
