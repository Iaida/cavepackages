﻿using UnityEngine;

[System.Serializable]
public class CaveProjectionArea
{
    public Vector3 TopRight;
    public Vector3 TopLeft;
    public Vector3 BottomRight;
    public Vector3 BottomLeft;
    public Vector3 Center;

    public Vector3 RightVector;
    public Vector3 UpVector;
    public Vector3 NormalVector;
    public Quaternion RotationTowards;


    private static readonly CaveProjectionArea FrontArea = new CaveProjectionArea(
        new Vector3(1.25f, 2.5f, 1.25f),
        new Vector3(-1.25f, 2.5f, 1.25f),
        new Vector3(1.25f, 0, 1.25f),
        new Vector3(-1.25f, 0, 1.25f));

    private static readonly CaveProjectionArea BottomArea = new CaveProjectionArea(
        new Vector3(1.25f, 0, 1.25f),
        new Vector3(-1.25f, 0, 1.25f),
        new Vector3(1.25f, 0, -1.25f),
        new Vector3(-1.25f, 0, -1.25f));

    private static readonly CaveProjectionArea RightArea = new CaveProjectionArea(
        new Vector3(1.25f, 2.5f, -1.25f),
        new Vector3(1.25f, 2.5f, 1.25f),
        new Vector3(1.25f, 0, -1.25f),
        new Vector3(1.25f, 0, 1.25f));

    private static readonly CaveProjectionArea LeftArea = new CaveProjectionArea(
        new Vector3(-1.25f, 2.5f, 1.25f),
        new Vector3(-1.25f, 2.5f, -1.25f),
        new Vector3(-1.25f, 0, 1.25f),
        new Vector3(-1.25f, 0, -1.25f));


    private CaveProjectionArea(Vector3 topRight, Vector3 topLeft, Vector3 bottomRight, Vector3 bottomLeft)
    {
        TopRight = topRight;
        TopLeft = topLeft;
        BottomRight = bottomRight;
        BottomLeft = bottomLeft;

        Center = (BottomRight + TopLeft) / 2;

        RightVector = (BottomRight - BottomLeft).normalized;
        UpVector = (TopLeft - BottomLeft).normalized;
        // needs the minus sign because Unity uses a left-handed coordinate system
        NormalVector = -Vector3.Cross(RightVector, UpVector).normalized;

        RotationTowards = Quaternion.LookRotation(-NormalVector, UpVector);
    }

    public static CaveProjectionArea CaveProjectionAreaFromMode(CaveMode.Mode mode)
    {
        switch (mode)
        {
            case CaveMode.Mode.Front:
                return FrontArea;
            case CaveMode.Mode.Right:
                return RightArea;
            case CaveMode.Mode.Left:
                return LeftArea;
            case CaveMode.Mode.Bottom:
                return BottomArea;
            default:
                throw new System.ArgumentException("only cave modes allowed");
        }
    }

    public CaveProjectionArea LocalToWorld(Transform transform)
    {
        return new CaveProjectionArea(
            transform.TransformPoint(TopRight),
            transform.TransformPoint(TopLeft),
            transform.TransformPoint(BottomRight),
            transform.TransformPoint(BottomLeft));
    }
}