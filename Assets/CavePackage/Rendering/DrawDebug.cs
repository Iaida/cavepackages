﻿using UnityEngine;

public static class DrawDebug
{
    public static void DrawDebugFrustum(Vector3 eyePos, float near, float far, CaveProjectionArea caveProjectionArea, float left, float right, float bottom, float top)
    {
        Vector3 nearCenter = eyePos - caveProjectionArea.NormalVector * near;

        Vector3 topRightNear = nearCenter + caveProjectionArea.RightVector * right + caveProjectionArea.UpVector * top;
        Vector3 topLeftNear = nearCenter + caveProjectionArea.RightVector * left + caveProjectionArea.UpVector * top;
        Vector3 bottomRightNear = nearCenter + caveProjectionArea.RightVector * right + caveProjectionArea.UpVector * bottom;
        Vector3 bottomLeftNear = nearCenter + caveProjectionArea.RightVector * left + caveProjectionArea.UpVector * bottom;

        DrawRectangle(topLeftNear, topRightNear, bottomRightNear, bottomLeftNear, Color.magenta);

        // Intercept theorem
        float factor = (far - near) / near;

        Vector3 topRightFar = topRightNear + (topRightNear - eyePos) * factor;
        Vector3 topLeftFar = topLeftNear + (topLeftNear - eyePos) * factor;
        Vector3 bottomRightFar = bottomRightNear + (bottomRightNear - eyePos) * factor;
        Vector3 bottomLeftFar = bottomLeftNear + (bottomLeftNear - eyePos) * factor;

        DrawRectangle(topLeftFar, topRightFar, bottomRightFar, bottomLeftFar, Color.magenta);

        Debug.DrawLine(topRightNear, topRightFar, Color.yellow);
        Debug.DrawLine(topLeftNear, topLeftFar, Color.yellow);
        Debug.DrawLine(bottomRightNear, bottomRightFar, Color.yellow);
        Debug.DrawLine(bottomLeftNear, bottomLeftFar, Color.yellow);
    }
    

    public static void DrawRectangle(Vector3 topLeft, Vector3 topRight, Vector3 bottomRight, Vector3 bottomLeft,
        Color color)
    {
        // 'DrawLine' draws from start to end
        Debug.DrawLine(topLeft, topRight, color);
        Debug.DrawLine(bottomLeft, bottomRight, color);
        Debug.DrawLine(topLeft, bottomLeft, color);
        Debug.DrawLine(topRight, bottomRight, color);
    }

    public static void DrawRectangle(CaveProjectionArea caveProjectionArea, Color color)
    {
        DrawRectangle(caveProjectionArea.TopLeft, caveProjectionArea.TopRight, caveProjectionArea.BottomRight,
            caveProjectionArea.BottomLeft, color);
    }

    public static void DrawLocalCoordinateSystem(Vector3 origin, Vector3 right, Vector3 up, Vector3 normal, float length)
    {
        Debug.DrawRay(origin, right * length, Color.red);
        Debug.DrawRay(origin, up * length, Color.green);
        Debug.DrawRay(origin, normal * length, Color.blue);
    }
}