﻿using UnityEngine;

// loosely based on: http://wiki.unity3d.com/index.php?title=FramesPerSecond

public class FPSDisplay : MonoBehaviour
{
    [SerializeField]
    private float _fps;
    [SerializeField]
    private float _milliseconds;  

    private float _deltaTime;
   
    private string _text;
    private Rect _rect;
    private GUIStyle _style;

    private void Start()
    {
        _style = new GUIStyle
        {
            alignment = TextAnchor.UpperLeft,
            fontSize = Screen.height * 2 / 100,
            normal = {textColor = Color.yellow}
        };
        _rect = new Rect(0, 0, Screen.width, Screen.height * 0.02f);
    }

    private void Update()
    {
        _deltaTime += (Time.unscaledDeltaTime - _deltaTime) * 0.1f;
        _milliseconds = _deltaTime * 1000.0f;
        _fps = 1.0f / _deltaTime;
    }

    private void OnGUI()
    {
        _text = string.Format("{0:0.0} ms ({1:0.} fps)", _milliseconds, _fps);
        GUI.Label(_rect, _text, _style);
    }
}
