﻿using System;
using UnityEngine;

// TODO : slight drag when applying fast head movements,
// caused by tracking average?

public class StereoRendering : MonoBehaviour
{
    public CaveSettings CaveSettings;
    
    public Camera CaveCamera;
    // Can be used for Server or additional Clients to provide an additional view
    // independent of the shutter glasses movement
    public Camera AlternativeCamera;

    

    // unity's default grey debug frustum is not representative of the actual matrices
    // use own debug frustum instead
    public bool ShowDebugFrustum;

    private Camera _stereoCameraRight;
    private Camera _stereoCameraLeft;

    private CaveProjectionArea _baseCaveProjectionArea;

    public enum Eye
    {
        Right,
        Left,
        None
    }

    // https://gamedev.stackexchange.com/questions/116009/in-unity-how-do-i-correctly-implement-the-singleton-pattern
    public static StereoRendering Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        else
            Instance = this;
    }

    private void Start()
    {
        SetVsync(CaveSettings.VSync);
        SetMaximumFrameRate(60);
        AssignCaveCamera();
        AddShutterGlassesTracking(CaveCamera.gameObject);

        if (CaveMode.Instance.IsInCaveMode())
        {
            ApplyCaveScreenSettings();

            // 'AlternativeCamera' is not used with Cave Clients
            if (AlternativeCamera != null)
                AlternativeCamera.gameObject.SetActive(false);

            GenerateStereoCameras();
            CaveCamera.enabled = false;
            _baseCaveProjectionArea = CaveProjectionArea.CaveProjectionAreaFromMode(CaveMode.Instance.CurrentMode);
        }
        else // Additional Client or Server
        {
            Screen.SetResolution(1280, 720, false);

            if (AlternativeCamera != null)
            {
                AlternativeCamera.gameObject.SetActive(true);

                // allow for tracking of the 'CaveCamera' object to continue,
                // but disable 'Camera' and 'AudioListener' components
                CaveCamera.enabled = false;
                CaveCamera.gameObject.GetComponent<AudioListener>().enabled = false;
            }
            else
                Debug.Log("No additional camera provided for 'AlternativeCamera', 'CaveCamera' remains active");
        }
    }

    private void LateUpdate()
    {
        if (CaveMode.Instance.IsInCaveMode() && CaveCamera.transform.hasChanged)
        {
            CaveCamera.transform.hasChanged = false;
            UpdateCameraFrustum(_stereoCameraRight, _baseCaveProjectionArea);
            UpdateCameraFrustum(_stereoCameraLeft, _baseCaveProjectionArea);
        }
    }

    private void ApplyCaveScreenSettings()
    {
        // PlayerPrefs and PlayerSettings in editor are not saved properly, 
        // https://answers.unity.com/questions/556589/resolution-not-saving-on-build.html
        // https://docs.unity3d.com/ScriptReference/PlayerPrefs.html

        // change settings at runtime instead
        // double screen height for stereo rendering, see 'GenerateStereoCameras'
        Screen.SetResolution(CaveSettings.ScreenWidth, CaveSettings.ScreenHeight * 2, CaveSettings.Fullscreen);
    }

    private void AssignCaveCamera()
    {
        // Check if already assigned properly
        if (CaveCamera)
            return;

        Debug.LogWarning("No camera assigned in inspector");

        if (Camera.main)
        {
            Debug.LogWarning("Found camera tagged 'maincamera'");
            CaveCamera = Camera.main;
        }
        else
        {
            Debug.LogWarning("No camera found tagged 'maincamera', taking first enabled camera");
            CaveCamera =  FindObjectOfType<Camera>();
        }
    }



    private void GenerateStereoCameras()
    {
        _stereoCameraRight = GenerateCamera(Eye.Right, "StereoCameraRight");
        _stereoCameraLeft = GenerateCamera(Eye.Left, "StereoCameraLeft");
    }

    public Camera GenerateCamera(Eye eye, string objectName)
    {
        GameObject go = new GameObject(objectName);

        // no audio listener, handled by mainCamera

        // add a copy of Camera component
        Camera cam = go.AddComponent<Camera>();
        cam.clearFlags = CaveCamera.clearFlags;
        cam.backgroundColor = CaveCamera.backgroundColor;
        cam.cullingMask = CaveCamera.cullingMask;
        cam.depth = CaveCamera.depth;
        

        cam.nearClipPlane = CaveSettings.NearPlane;
        cam.farClipPlane = CaveSettings.FarPlane;

        cam.rect = ViewportRectangleFromEye(eye);

        Transform camTransform = go.transform;
        Transform caveCamTransform = CaveCamera.transform;

        camTransform.SetPositionAndRotation(caveCamTransform.position, caveCamTransform.rotation);

        // child object transforms of 'MainCamera' are updated accordingly
        // when 'MainCamera's transform is updated by the tracking of the shutter glasses
        camTransform.parent = caveCamTransform;

        camTransform.localPosition = EyeOffset(eye);

        return cam;
    }





    private Vector3 EyeOffset(Eye eye)
    {
        switch (eye)
        {
            case Eye.Right:
                return new Vector3(
                    CaveSettings.DistanceBetweenEyesMeter / 2.0f, 
                    0.0f, 
                    -CaveSettings.EyeLensToGlassesDistanceMeter);
            case Eye.Left:
                return new Vector3(
                    -CaveSettings.DistanceBetweenEyesMeter / 2.0f,
                    0.0f, 
                    -CaveSettings.EyeLensToGlassesDistanceMeter);
            case Eye.None:
                return new Vector3(
                    0.0f, 
                    0.0f, 
                    -CaveSettings.EyeLensToGlassesDistanceMeter);
            default:
                throw new ArgumentOutOfRangeException("eye", eye, null);
        }
    }

    private static Rect ViewportRectangleFromEye(Eye eye)
    {
        switch (eye)
        {
            // aspect ratio: 1:2
            // example resolution: 1600x3200
            // left camera above right camera -> left camera y-value starts at half height
            // both cameras use the full screen width -> width-value is 1
            // both cameras use half of the screen height -> height value is 0.5
            case Eye.Right:
                return new Rect(0.0f, 0.0f, 1.0f, 0.5f);
            case Eye.Left:
                return new Rect(0.0f, 0.5f, 1.0f, 0.5f);
            // default across entire screen
            case Eye.None:
                return new Rect(0.0f, 0.0f, 1.0f, 1.0f);
            default:
                throw new ArgumentOutOfRangeException("eye", eye, null);
        }
    }

    // based on: https://en.wikibooks.org/wiki/Cg_Programming/Unity/Projection_for_Virtual_Reality
    // same results as: https://forum.unity.com/threads/code-sample-off-center-projection-code-for-vr-cave-or-just-for-fun.142383/
    // same results as Planetarium project's matrix calculation
    public void UpdateCameraFrustum(Camera cam, CaveProjectionArea baseCaveProjectionArea)
    {
        // check if MainCamera has a parent object
        // parent present   -> transform the baseCaveProjectionArea from local to world space coordinates
        // no parent        -> baseCaveProjectionArea' local coordinates are already in world space coordinates
        CaveProjectionArea caveProjectionArea = CaveCamera.transform.parent
            ? baseCaveProjectionArea.LocalToWorld(CaveCamera.transform.parent)
            : baseCaveProjectionArea;

        // ok
        //DrawDebug.DrawRectangle(caveProjectionArea, Color.magenta);
        //DrawDebug.DrawLocalCoordinateSystem(caveProjectionArea.Center,
        //    caveProjectionArea.RightVector, caveProjectionArea.UpVector, caveProjectionArea.NormalVector,
        //    500.0f);

        // ok, stays unit length
        //Debug.Log("caveProjectionArea's RightVector: \n" + caveProjectionArea.RightVector);
        //Debug.Log("caveProjectionArea's UpVector: \n" + caveProjectionArea.UpVector);
        //Debug.Log("caveProjectionArea's NormalVector: \n" + caveProjectionArea.NormalVector);


        //Vector3 eyePos = MainCamera.transform.parent.InverseTransformPoint(cam.transform.position);
        Vector3 eyePos = cam.transform.position;
        //Debug.Log(cam.gameObject.name + "'s eyePos: \n" + eyePos);

        Vector3 eyeToBottomLeft = caveProjectionArea.BottomLeft - eyePos;
        Vector3 eyeToBottomRight = caveProjectionArea.BottomRight - eyePos;
        Vector3 eyeToTopLeft = caveProjectionArea.TopLeft - eyePos;

        // ok
        //Debug.DrawLine(eyePos, caveProjectionArea.BottomLeft);
        //Debug.DrawLine(eyePos, caveProjectionArea.BottomRight);
        //Debug.DrawLine(eyePos, caveProjectionArea.TopLeft);

        // Careful: 'distance' can be 0 when the default camera position lies inside one of the projection area's planes.
        // The camera then renders nothing and 'IsMatrixValid'-errors occur on cave clients when setting projection matrix
        float distance = -Vector3.Dot(eyeToBottomLeft, caveProjectionArea.NormalVector);

        float left = Vector3.Dot(caveProjectionArea.RightVector, eyeToBottomLeft) * cam.nearClipPlane / distance;
        float right = Vector3.Dot(caveProjectionArea.RightVector, eyeToBottomRight) * cam.nearClipPlane / distance;
        float bottom = Vector3.Dot(caveProjectionArea.UpVector, eyeToBottomLeft) * cam.nearClipPlane / distance;
        float top = Vector3.Dot(caveProjectionArea.UpVector, eyeToTopLeft) * cam.nearClipPlane / distance;

        // set matrices

        cam.projectionMatrix = PerspectiveOffCenter(
            left, right,
            bottom, top,
            cam.nearClipPlane, cam.farClipPlane);
        //Debug.Log(cam.gameObject.name + "'s projectionMatrix: \n" + cam.projectionMatrix);

        if (ShowDebugFrustum)
        {
            DrawDebug.DrawDebugFrustum(eyePos, cam.nearClipPlane, cam.farClipPlane,
                caveProjectionArea, left, right, bottom, top);
        }


        // The original paper puts everything into the projection matrix
        // (i.e. sets it to p * rm * tm and the other matrix to the identity), 
        // but this doesn't appear to work with Unity's shadow maps.

        // not usable for parent objects
        //cam.worldToCameraMatrix = CalcWorldToCameraMatrix(
        //    eyePos,
        //    caveProjectionArea.RightVector,
        //    caveProjectionArea.UpVector,
        //    caveProjectionArea.NormalVector);

        // workaround for parent object
        //Quaternion localRot = LocalRotationFromMode(CaveMode.Instance.CurrentMode);
        //Quaternion worldRot = MainCamera.transform.parent
        //    ? LocalToWorldRotation(localRot, MainCamera.transform.parent)
        //    : localRot;

        Quaternion worldRot = caveProjectionArea.RotationTowards;

        cam.worldToCameraMatrix = GenerateWorldToCameraMatrix(eyePos, worldRot);



        //Debug.Log("MainCamera's eulerAngles: \n" + MainCamera.transform.rotation.eulerAngles);
        //Debug.Log("MainCamera's eulerAngles from worldToCameraMatrix: \n" + 
        //          QuaternionFromWorldToCameraMatrix(MainCamera.worldToCameraMatrix).eulerAngles);
        //Debug.Log(cam.gameObject.name + "'s eulerAngles from worldToCameraMatrix: \n" +
        //          QuaternionFromWorldToCameraMatrix(cam.worldToCameraMatrix).eulerAngles);


        //Debug.Log(cam.gameObject.name + "'s worldToCameraMatrix: \n" + cam.worldToCameraMatrix);
        //Debug.Log(MainCamera.gameObject.name + "'s worldToCameraMatrix: \n" + MainCamera.worldToCameraMatrix);
        //Debug.Log(MainCamera.gameObject.name + "'s worldToCameraMatrix calculated: \n" + GenerateWorldToCameraMatrix(MainCamera.gameObject));

        //Debug.Log(cam.gameObject.name + "'s localEulerAngles: " + cam.transform.localEulerAngles);

        // set unity's camera rotation to allow for correct frustum culling, look at center of screen
        cam.transform.LookAt(caveProjectionArea.Center, caveProjectionArea.UpVector);
        // ok
        //Debug.DrawLine(eyePos, caveProjectionArea.Center, Color.magenta);
        //Debug.DrawRay(eyePos, caveProjectionArea.UpVector * 250, Color.yellow);

        //Debug.Log(cam.gameObject.name + "'s localEulerAngles: " + cam.transform.localEulerAngles);


        // set unity's field of view to allow for correct frustum culling
        // extract vertical fov from projection matrix
        cam.fieldOfView = VerticalFovFromProjectionMatrix(cam.projectionMatrix);
    }

    // based on: https://docs.unity3d.com/ScriptReference/Camera-projectionMatrix.html
    // Set an off-center projection, where perspective's vanishing point is not necessarily in the center of the screen.
    // left/right/top/bottom define near plane size, i.e. how offset are corners of camera's near plane.
    private static Matrix4x4 PerspectiveOffCenter(float left, float right, float bottom, float top, float near,
        float far)
    {
        Matrix4x4 m = Matrix4x4.zero;
        m[0, 0] = 2.0f * near / (right - left);
        m[0, 2] = (right + left) / (right - left);
        m[1, 1] = 2.0f * near / (top - bottom);
        m[1, 2] = (top + bottom) / (top - bottom);
        m[2, 2] = -(far + near) / (far - near);
        m[2, 3] = -(2.0f * far * near) / (far - near);
        m[3, 2] = -1.0f;
        return m;
    }

    // https://forum.unity.com/threads/how-can-i-extract-the-fov-information-from-the-projection-matrix.262352/
    private static float VerticalFovFromProjectionMatrix(Matrix4x4 projectionMatrix)
    {
        // projectionMatrix[1, 1] == 2.0f * near / (top - bottom);
        // tan angleRad = opposite / adjacent
        // opposite: height / 2
        // adjacent: near
        // result is half of vertical fov -> * 2
        return Mathf.Rad2Deg * Mathf.Atan(1.0f / projectionMatrix[1, 1]) * 2.0f;
    }

    private static void SetVsync(bool active)
    {
        // 'vSyncCount' is an int
        // 0 -> Don't Sync 
        // 1 -> Every V Blank (60fps)
        // problem: when framerate goes below 60, it randomly gets capped at 30
        // solution for games <60fps: use 'SetMaximumFrameRate' instead
        QualitySettings.vSyncCount = active ? 1 : 0;
    }

    private static void SetMaximumFrameRate(int fps)
    {
        Application.targetFrameRate = fps;
    }


    private static void AddShutterGlassesTracking(GameObject go)
    {
        TrackerObject tos = go.AddComponent<TrackerObject>();
        tos.Id = TrackerId.ShutterGlasses;
        tos.ToggleChildren = false;
    }


    private static Matrix4x4 GenerateWorldToCameraMatrix(Vector3 worldSpacePosition, Quaternion worldSpaceRotation)
    {
        // originally based on: https://forum.unity.com/threads/reproducing-cameras-worldtocameramatrix.365645/

        //Matrix4x4 m = Matrix4x4.TRS(worldSpacePosition, worldSpaceRotation, Vector3.one);
        //m = Matrix4x4.Inverse(m);
        ////Debug.Log("matrix function inverted: \n" + m);

        //Quaternion invertedRotation = Quaternion.Inverse(worldSpaceRotation);
        //Matrix4x4 m = Matrix4x4.TRS(invertedRotation * -worldSpacePosition, invertedRotation, Vector3.one);
        ////Debug.Log("parameters inverted: \n" + m);

        Matrix4x4 m = Matrix4x4.Rotate(Quaternion.Inverse(worldSpaceRotation)) *
                      Matrix4x4.Translate(-worldSpacePosition);
        //Debug.Log("self composed: \n" + m);

        // unity -> opengl z-axis
        m.m20 *= -1f;
        m.m21 *= -1f;
        m.m22 *= -1f;
        m.m23 *= -1f;
        return m;
    }

    private static Quaternion QuaternionFromMatrix(Matrix4x4 m)
    {
        // https://answers.unity.com/questions/402280/how-to-decompose-a-trs-matrix.html
        return Quaternion.LookRotation(m.GetColumn(2), m.GetColumn(1));
    }

    // Old Version
    private static Quaternion QuaternionFromWorldToCameraMatrix(Matrix4x4 worldToCameraMatrix)
    {
        // based on answer by 'Leigh': https://answers.unity.com/questions/11363/converting-matrix4x4-to-quaternion-vector3.html
        return QuaternionFromMatrix(worldToCameraMatrix.inverse * Matrix4x4.Scale(new Vector3(1, 1, -1)));
    }

    // Old Version
    private static Quaternion LocalToWorldRotation(Quaternion localRotation, Transform localTransform)
    {
        // https://answers.unity.com/questions/275565/what-is-the-rotation-equivalent-of-inversetransfor.html
        return localTransform.rotation * localRotation;
    }

    // Old Version
    private static Quaternion LocalRotationFromMode(CaveMode.Mode mode)
    {
        switch (mode)
        {
            case CaveMode.Mode.Front:
                return Quaternion.Euler(new Vector3(0, 0, 0));
            case CaveMode.Mode.Right:
                return Quaternion.Euler(new Vector3(0, 90, 0));
            case CaveMode.Mode.Left:
                return Quaternion.Euler(new Vector3(0, -90, 0));
            case CaveMode.Mode.Bottom:
                return Quaternion.Euler(new Vector3(90, 0, 0));
            default:
                throw new ArgumentOutOfRangeException("mode", mode, "only cave modes allowed");
        }
    }

    // Old Version
    private static Matrix4x4 CalcWorldToCameraMatrix(Vector3 pos, Vector3 right, Vector3 up, Vector3 normal)
    {
        Matrix4x4 rotationMatrix = RotationMatrixFromThreeAxes(right, up, normal);
        Matrix4x4 translationMatrix = Matrix4x4.Translate(-pos);
        return rotationMatrix * translationMatrix;
    }

    // Old Version
    private static Matrix4x4 RotationMatrixFromThreeAxes(Vector3 a, Vector3 b, Vector3 c)
    {
        return MathUtilities.MatrixFromColumns(a, b, c, new Vector4(0, 0, 0, 1));
    }
}
