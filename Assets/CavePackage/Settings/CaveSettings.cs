﻿using System;
using System.Linq;
using UnityEngine;
using System.Reflection;

[CreateAssetMenu(fileName = "CaveSettings", menuName = "CavePackage/Settings", order = 1)]
public class CaveSettings : ScriptableObject
{
    [Header("IP Addresses")]
    public string ServerIp = "172.16.9.42";
    [Space(10)]
    public string CaveFrontIp = "172.16.9.121";
    public string CaveDownIp = "172.16.9.122";
    public string CaveRightIp = "172.16.9.123";
    public string CaveLeftIp = "172.16.9.124";

    [Space(10)]

    [Header("Screen")]
    public bool Fullscreen;
    public bool VSync = true;
    public int ScreenWidth = 1600;
    public int ScreenHeight = 1600;

    [Space(10)]

    [Header("Rendering")]
    [Tooltip("Stereo effect disappears with a distance of 0")]
    public float DistanceBetweenEyesMeter = 0.063f;
    public float EyeLensToGlassesDistanceMeter = 0.035f;

    [Tooltip("Near and Far plane ratio of 1:10000")]
    public float NearPlane = 0.035f;
    [Tooltip("Near and Far plane ratio of 1:10000")]
    public float FarPlane = 350;


    [Space(10)]

    [Header("Tracking")]
    public int TrackingPort = 10000;
    public string TrackingIp = "172.16.9.132";
    public int TrackingMulticastPort = 10000;
    public string TrackingMulticastIp = "224.0.1.0";

    [Space(10)]

    [Header("Wii Server")]
    public string WiiServerIp = "172.16.9.42";
    public int WiiServerPort = 1300;
    public string WiimoteIdentifier = "wm";
    [Tooltip("Can be used instead of 'WiimoteIdentifier'")]
    public string WiimoteMac = "00-19-1D-C5-A3-94";
    public string BalanceBoardIdentifier = "bb";
    [Tooltip("Can be used instead of 'BalanceBoardIdentifier'")]
    public string BalanceBoardMac = "34-AF-2C-E7-60-DF";

    private static readonly FieldInfo[] SettingsFieldInfos = typeof(CaveSettings).GetFields();

    // access: right-click on top of inspector where icon and name can be seen
    [ContextMenu("Assign Local IP to Server IP")]
    public void AssignLocalIpToServerIp()
    {
        Debug.Log("Changing server ip from " + ServerIp + " to local ip " + IpUtilities.LocalIp +
                  " in CaveNetworkManager");
        ServerIp = IpUtilities.LocalIp;
    }

    

    // 'memberString' syntax:   -'case-insensitive field name':'new value'
    // 'memberString' example:  -ServerIp:172.16.9.42 OR -serverip:172.16.9.42 OR -SERVERIP:172.16.9.42
    public void ParseSettingsMember(string memberString)
    {
        FieldInfo fieldInfo = SettingsFieldInfos.FirstOrDefault(
            fi => memberString.StartsWith('-' + fi.Name + ':', StringComparison.InvariantCultureIgnoreCase));

        if (fieldInfo == null)
            return;

        var splitResult = StringUtilities.Split(memberString, ':');
        if (splitResult.Length != 2)
        {
            Debug.LogError("Option 'case sensitive field name' was not followed by ':' and the new value");
            return;
        }

        try
        {
            object newValue = Convert.ChangeType(splitResult[1], fieldInfo.FieldType);
            fieldInfo.SetValue(this, newValue);
        }
        catch (Exception e)
        {
            Debug.LogError(e);
            Debug.LogError("Exception thrown trying to cast from " + splitResult[1] + " to " + fieldInfo.FieldType);
        }
    }
}
