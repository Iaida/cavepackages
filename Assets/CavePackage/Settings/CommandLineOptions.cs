﻿using System;
using UnityEngine;

public class CommandLineOptions : MonoBehaviour
{
    public CaveSettings CaveSettings;
    public CaveMode CaveMode;
    public bool PrintParameters;

    private void Awake()
    {
        string[] args = Environment.GetCommandLineArgs();
        // skip first element, executable file name
        for (int i = 0; i < args.Length; i++)
        {
            if (PrintParameters)
                Debug.Log("ARG " + i + ": " + args[i]);

            CaveMode.ParseMode(args[i]);
            CaveSettings.ParseSettingsMember(args[i]);
        }
    }
}
