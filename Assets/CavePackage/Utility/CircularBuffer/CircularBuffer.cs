﻿// https://en.wikipedia.org/wiki/Circular_buffer

using System;
using System.Collections;
using System.Collections.Generic;

public class CircularBuffer<T> : IEnumerable<T>
{
    public int Capacity { get; private set; }
    public int Count { get; private set; }

    private int _startIndex;
    
    private T[] _buffer;

    public CircularBuffer(int capacity)
    {
        Capacity = capacity;
        _buffer = new T[Capacity];
        Count = 0;
        _startIndex = 0;
    }

    public void Add(T element)
    {
        _buffer[IncreaseIndex(_startIndex, Count)] = element;
        // if the buffer is full, assignment will replace the old value at startIndex
        if (Count == Capacity)
            _startIndex = IncrementIndex(_startIndex);
        else
            Count++;
    }

    public T GetElementAtIndex(int index)
    {
        if (index < 0)
            throw new ArgumentOutOfRangeException("index", "index negative");
        if (index >= Count)
            throw new ArgumentOutOfRangeException("index", "index too large");

        return _buffer[IncreaseIndex(_startIndex, index)];
    }

    private int IncreaseIndex(int index, int steps)
    {
        return (index + steps) % Capacity;
    }

    private int IncrementIndex(int index)
    {
        return IncreaseIndex(index, 1);
    }

    public void Clear(bool resetInternalArray = false)
    {
        if (resetInternalArray)
            _buffer = new T[Capacity];
        Count = 0;
        _startIndex = 0;
    }

    public IEnumerator<T> GetEnumerator()
    {
        int index = _startIndex;
        for (int i = 0; i < Count; i++)
        {
            T currentElement = _buffer[index];
            index = IncrementIndex(index);
            yield return currentElement;
        }   
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}

