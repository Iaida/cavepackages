﻿using UnityEngine;

public class CircularBufferTest : MonoBehaviour
{
    public CircularBuffer<int> CircularBuffer;
    public int Capacity;
    private int _counter;

    // Use this for initialization
    private void Start()
    {
        Capacity = 10;
        CircularBuffer = new CircularBuffer<int>(Capacity);

        AddToBuffer(15);
        PrintBufferContent();
        PrintAtIndex(5);
        CircularBuffer.Clear();
        PrintBufferContent();

    }

    private void AddToBuffer(int numElements)
    {
        for (int i = 0; i < numElements; i++)
        {
            CircularBuffer.Add(_counter++);
        }
    }

    private void PrintAtIndex(int index)
    {
        Debug.Log("Element at index: " + index + ": " + CircularBuffer.GetElementAtIndex(index));
    }

    private void PrintBufferContent()
    {
        string content = "";
        foreach (int i in CircularBuffer)
        {
            content += i + ", ";
        }

        Debug.Log("Content: " + content);
    }
}