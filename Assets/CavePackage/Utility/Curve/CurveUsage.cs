﻿using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class CurveUsage : MonoBehaviour
{
    public LineRenderer LineRenderer;
    public Transform P0;
    public Transform P1;
    public Transform P2;
    public Transform P3;

    public int PointSamples = 100;

    private void Update()
    {
        if (P0.hasChanged || P1.hasChanged || P2.hasChanged || P3.hasChanged)
            UpdateLineRenderer();
    }

    private void UpdateLineRenderer()
    {
        Vector3[] positions = new Vector3[PointSamples];

        for (int i = 0; i < PointSamples; i++)
        {
            float fraction = i / (float)PointSamples;
            positions[i] = CurveUtilities.CubicBezier(P0.position, P1.position, P2.position, P3.position, fraction);
        }

        LineRenderer.positionCount = PointSamples;
        LineRenderer.SetPositions(positions);
    }
}
