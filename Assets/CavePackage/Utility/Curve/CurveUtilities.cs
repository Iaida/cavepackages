﻿using UnityEngine;

public static class CurveUtilities
{
    // http://intern.fh-wedel.de/~bo/db/handouts/intern/ws17/polynome-2.pdf
    // http://intern.fh-wedel.de/fileadmin/mitarbeiter/ne/CG2/Splineflaechen.pdf
    // http://intern.fh-wedel.de/fileadmin/mitarbeiter/ne/CG2/SplinesUndKonvexeHuelle.pdf

    // http://www.cubic.org/docs/hermite.htm
    // http://www.cs.cmu.edu/afs/cs/academic/class/15462-s10/www/lec-slides/lec06.pdf
    // http://graphics.stanford.edu/courses/cs148-09/lectures/splines.pdf

    public static readonly Matrix4x4 HermiteSplineInterpolationMatrix = MathUtilities.MatrixFromRows(
        new Vector4(2, -2, 1, 1),
        new Vector4(-3, 3, -2, -1),
        new Vector4(0, 0, 1, 0),
        new Vector4(1, 0, 0, 0));

    public static readonly Matrix4x4 BezierInterpolationMatrix = MathUtilities.MatrixFromRows(
        new Vector4(-1, 3, -3, 1),
        new Vector4(3, -6, 3, 0),
        new Vector4(-3, 3, 0, 0),
        new Vector4(1, 0, 0, 0));

    public static readonly Matrix4x4 CatmullRomInterpolationMatrix = MathUtilities.MatrixFromRows(
        0.5f * new Vector4(-1, 3, -3, 1),
        0.5f * new Vector4(2, -5, 4, -1),
        0.5f * new Vector4(-1, 0, 1, 0),
        0.5f * new Vector4(0, 2, 0, 0));


    public static readonly Matrix4x4 SplineInterpolationMatrix = MathUtilities.MatrixFromRows(
        (1f / 6f) * new Vector4(-1, 3, -3, 1),
        (1f / 6f) * new Vector4(3, -6, 3, 0),
        (1f / 6f) * new Vector4(-3, 0, 3, 0),
        (1f / 6f) * new Vector4(1, 4, 1, 0));

    private static Vector4 MonomialVector(float t)
    {
        return new Vector4(t * t * t, t * t, t, 1);
    }

    private static Vector4 InterpolationVector(Matrix4x4 m, float t)
    {
        return m * MonomialVector(t);
    }

    // can be used with CurveUtilities' static interpolation matrices
    public static Vector3 InterpolateWithMatrix(Vector3 a, Vector3 b, Vector3 c, Vector3 d, float t, Matrix4x4 interpolationMatrix)
    {
        // generate matrix from points instead of explicit multiple dot products
        return MathUtilities.MatrixFromColumns(a, b, c, d) * InterpolationVector(interpolationMatrix, t);
    }


    // https://en.wikipedia.org/wiki/B%C3%A9zier_curve#Cubic_B%C3%A9zier_curves
    public static Vector3 CubicBezier(Vector3 a, Vector3 b, Vector3 c, Vector3 d, float t)
    {
        float tt = t * t;
        float ttt = tt * t;
        float u = (1 - t);
        float uu = u * u;
        float uuu = uu * u;

        return uuu * a +
               3 * uu * t * b +
               3 * u * tt * c +
               ttt * d;
    }
}