﻿using UnityEditor;
using UnityEngine;

public class CavePackageMenuBar : EditorWindow
{
    private const string SettingsPath = "Assets/CavePackage/Settings/CaveSettings.asset";
    private const string CaveCubePath = "Assets/CavePackage/CorePrefabs/CaveCube.prefab";
    private const string CaveManagementPath = "Assets/CavePackage/CorePrefabs/CaveManagement.prefab";

    [MenuItem("CavePackage/Cave Setup")]
    private static void DoCaveSetup()
    {
        DisableResolutionDialog();
        AddCaveManagement();
        AddCaveCube();
    }

    [MenuItem("CavePackage/Add CaveManagement to Scene")]
    private static void AddCaveManagement()
    {
        InstantiatePrefabFromPath(CaveManagementPath);
    }

    [MenuItem("CavePackage/Add CaveCube to Scene")]
    private static void AddCaveCube()
    {
        InstantiatePrefabFromPath(CaveCubePath);
    }

    private static void InstantiatePrefabFromPath(string filePath)
    {
        GameObject go = AssetDatabase.LoadAssetAtPath<GameObject>(filePath);
        if (!go)
        {
            Debug.LogError("Failed loading Prefab from " + filePath);
            return;
        }

        if (GameObject.Find(go.name))
        {
            Debug.LogError("Object named " + go.name + " already exists");
            return;
        }
            
        PrefabUtility.InstantiatePrefab(go);
    }

    [MenuItem("CavePackage/Disable Resolution Dialog")]
    private static void DisableResolutionDialog()
    {
        PlayerSettings.displayResolutionDialog = ResolutionDialogSetting.Disabled;
        Debug.Log("Disabled Resolution Dialog");
    }

    [MenuItem("CavePackage/Assign Local IP to Server IP")]
    private static void AssignLocalIpToServerIp()
    {
        CaveSettings s = AssetDatabase.LoadAssetAtPath<CaveSettings>(SettingsPath);
        if (s)
            s.AssignLocalIpToServerIp();
        else
            Debug.LogError("Failed loading CaveSettings from " + SettingsPath);
    }

    [MenuItem("CavePackage/Assign Cave Near and Far Plane Values to Selected Camera")]
    private static void AssignNearFar()
    {
        CaveSettings s = AssetDatabase.LoadAssetAtPath<CaveSettings>(SettingsPath);
        if (!s)
        {
            Debug.LogError("Failed loading CaveSettings from " + SettingsPath);
            return;
        }

        Camera c = Selection.activeGameObject.GetComponent<Camera>();
        c.nearClipPlane = s.NearPlane;
        c.farClipPlane = s.FarPlane;
    }

    [MenuItem("CavePackage/Assign Cave Near and Far Plane Values to Selected Camera", true)]
    private static bool ValidateAssignNearFar()
    {
        return Selection.activeGameObject != null &&
               Selection.activeGameObject.GetComponent<Camera>() != null;
    }
}
