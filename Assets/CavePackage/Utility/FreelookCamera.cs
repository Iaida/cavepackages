﻿using UnityEngine;

// based on: http://wiki.unity3d.com/index.php/FlyThrough

public class FreelookCamera : MonoBehaviour
{
    public float LookSpeed = 10.0f;
    public float MoveSpeed = 15.0f;

    private float _rotationX;
    private float _rotationY;

    private void Update()
    {
        _rotationX += Input.GetAxis("Mouse X") * LookSpeed;
        _rotationY += Input.GetAxis("Mouse Y") * LookSpeed;
        _rotationY = Mathf.Clamp(_rotationY, -90, 90);
        

        transform.localRotation = Quaternion.AngleAxis(_rotationX, Vector3.up);
        transform.localRotation *= Quaternion.AngleAxis(_rotationY, Vector3.left);

        transform.position += transform.forward * MoveSpeed * Input.GetAxis("Vertical");
        transform.position += transform.right * MoveSpeed * Input.GetAxis("Horizontal");
    }
}
