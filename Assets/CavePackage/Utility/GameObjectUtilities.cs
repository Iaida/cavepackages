﻿using UnityEngine;

public static class GameObjectUtilities
{
    public static void RebuildMeshCollidersAtRuntime()
    {
        MeshCollider[] meshColliders = Object.FindObjectsOfType<MeshCollider>();

        Debug.Log("Rebuilding " + meshColliders.Length + " MeshCollider component(s)...");

        foreach (MeshCollider mc in meshColliders)
        {
            RebuildMeshCollider(mc);
        }

        Debug.Log("Finished rebuilding MeshCollider components");
    }

    public static void RebuildMeshCollider(MeshCollider mc)
    {
        MeshFilter mf = mc.GetComponent<MeshFilter>();
        if (mf == null)
        {
            Debug.Log(mc.gameObject.name + " is missing a MeshFilter component");
            return;
        }

        // https://forum.unity.com/threads/how-to-update-a-mesh-collider.32467/
        mc.sharedMesh = null;
        mc.sharedMesh = mf.mesh;
    }

    // https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/extension-methods

    public static bool CheckForComponent<T>(this GameObject gameObject) where T : Component
    {
        return gameObject.GetComponent<T>() != null;
    }

    public static bool CheckForComponentAndPrintError<T>(this GameObject gameObject) where T : Component
    {
        bool result = CheckForComponent<T>(gameObject);
        if (!result)
            Debug.LogError("GameObject " + gameObject.name + "does not have " + typeof(T).Name + " component");
        return result;
    }

}
