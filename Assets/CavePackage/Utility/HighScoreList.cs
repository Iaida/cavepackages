﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[System.Serializable]
public struct HighScore
{
    public int Score;
    public string Name;

    public HighScore(int score, string name)
    {
        Score = score;
        Name = name;
    }

    public HighScore(int score)
    {
        Score = score;
        // timestamp
        Name = DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("de-DE"));
    }
}

[System.Serializable]
public class HighScoreList
{
    // allows for duplicate score and name values
    // highest score at [0]
    [SerializeField]
    private List<HighScore> _list = new List<HighScore>();

    private readonly string _filepath;


    // example 'filepath': Application.persistentDataPath + "/HighScore.dat"
    public HighScoreList(string filepath)
    {
        _filepath = filepath;
        Load();
    }

    // for RPC calls
    public HighScoreList(HighScore[] sortedArray)
    {
        _list = new List<HighScore>(sortedArray);
    }

    // for RPC calls
    public HighScore[] ToSortedArray()
    {
        return _list.ToArray();
    }
    

    public void Add(HighScore highScore)
    {
        // insert at 'Count' increases size
        _list.Insert(InsertionIndexFromScore(highScore.Score), highScore);
        Save();
    }

    private int InsertionIndexFromScore(int newScore)
    {
        int index = _list.FindIndex(hs => newScore >= hs.Score);
        // if not found, index is -1, insert at 'Count'
        return index == -1 ? _list.Count : index;
    }


    public void PrintKeysAndValues()
    {
        for (int i = 0; i < _list.Count; i++)
        {
            Debug.Log("Index: " + i +
                      ", Score: " + _list[i].Score +
                      ", Name: " + _list[i].Name);
        }
    }

    public List<HighScore> GetList()
    {
        return _list;
    }

    public List<HighScore> GetTopList(int top)
    {
        return _list.GetRange(0, top);
    }

    
    // source: https://www.youtube.com/watch?v=J6FfcJpbPXE
    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(_filepath, FileMode.OpenOrCreate);
        bf.Serialize(file, _list);
        file.Close();
        Debug.Log("HighScore file saved to " + _filepath);
    }
    
    public bool Load()
    {
        Debug.Log("Loading HighScore file at " + _filepath);

        if (!File.Exists(_filepath))
        {
            Debug.LogWarning("HighScore file not found at " + _filepath + " - Empty HighScore List");
            return false;
        }

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(_filepath, FileMode.Open);
        _list = (List<HighScore>)bf.Deserialize(file);
        file.Close();
        return true;
    }

}