﻿#pragma warning disable 0618 // UNet Deprecation warning disabled

using System.Net;
using UnityEngine.Networking;

public static class IpUtilities
{
    public static readonly string LocalIp = GetLocalIp();

    // https://stackoverflow.com/questions/6803073/get-local-ip-address/6803109#6803109
    private static string GetLocalIp()
    {
        IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());      

        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                return ip.ToString();
        }
        return null;
    }

    public static string FormattedIp(this NetworkConnection c)
    {
        // IPv4 - mapped IPv6 addresses have a prefix
        const string mappingPrefix = "::ffff:";

        // remove prefix if present
        return c.address.StartsWith(mappingPrefix) ? 
            c.address.Substring(mappingPrefix.Length) : 
            c.address;
    }
}
