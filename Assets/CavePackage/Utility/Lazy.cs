﻿// System.Lazy only available since .Net version 4.0
// https://stackoverflow.com/questions/3207580/implementation-of-lazyt-for-net-3-5

using System;

public class Lazy<T>
{
    private readonly Func<T> _initializer;
    private bool _isValueCreated;
    private T _value;

    public Lazy(Func<T> initializer)
    {
        if (initializer == null)
            throw new ArgumentNullException("initializer");
        _initializer = initializer;
    }

    public bool IsValueCreated
    {
        get { return _isValueCreated; }
    }

    public T Value
    {
        get
        {
            if (!_isValueCreated)
            {
                _value = _initializer();
                _isValueCreated = true;
            }
            return _value;
        }
    }
}