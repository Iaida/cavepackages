﻿using UnityEngine;

public static class MathUtilities
{
    // using matrices for swapping rows and columns: 
    // https://unapologetic.wordpress.com/2009/08/27/elementary-row-and-column-operations/
    public static Matrix4x4 GenerateSwapMatrix(int indexA, int indexB)
    {
        Matrix4x4 matrix = Matrix4x4.identity;
        matrix[indexA, indexA] = 0;
        matrix[indexB, indexB] = 0;
        matrix[indexA, indexB] = 1;
        matrix[indexB, indexA] = 1;
        return matrix;
    }

    public static Matrix4x4 AddMatrices(Matrix4x4 a, Matrix4x4 b)
    {
        // column-wise addition
        return MatrixFromColumns(
            a.GetColumn(0) + b.GetColumn(0),
            a.GetColumn(1) + b.GetColumn(1),
            a.GetColumn(2) + b.GetColumn(2),
            a.GetColumn(3) + b.GetColumn(3));
    }

    public static Matrix4x4 ScaleMatrixComponents(Matrix4x4 m, float factor)
    {
        // column-wise vector scaling
        return MatrixFromColumns(
            m.GetColumn(0) * factor,
            m.GetColumn(1) * factor,
            m.GetColumn(2) * factor,
            m.GetColumn(3) * factor);
    }

    public static Vector3 ScaleVector(Vector3 input, Vector3 inputMin, Vector3 inputMax, Vector3 outputMin,
        Vector3 outputMax)
    {
        return new Vector3(
            ScaleFloat(input.x, inputMin.x, inputMax.x, outputMin.x, outputMax.x),
            ScaleFloat(input.y, inputMin.y, inputMax.y, outputMin.y, outputMax.y),
            ScaleFloat(input.z, inputMin.z, inputMax.z, outputMin.z, outputMax.z));
    }

    // https://stackoverflow.com/questions/5294955/how-to-scale-down-a-range-of-numbers-with-a-known-min-and-max-value/5295202#5295202
    public static float ScaleFloat(float input, float inputMin, float inputMax, float outputMin, float outputMax)
    {
        return (outputMax - outputMin) * (input - inputMin) / (inputMax - inputMin) + outputMin;
    }

    // needs values of 't' ranging from 0 to 2 for the backwards direction of 'Mathf.PingPong'
    public static Vector3 LerpPingPong(Vector3 start, Vector3 end, float t)
    {
        // https://docs.unity3d.com/ScriptReference/Mathf.PingPong.html
        return Vector3.Lerp(start, end, Mathf.PingPong(t, 1.0f));
    }

    public static Matrix4x4 MatrixFromColumns(Vector4 firstColumn, Vector4 secondColumn,
        Vector4 thirdColumn, Vector4 fourthColumn)
    {
        Matrix4x4 m = new Matrix4x4();
        m.SetColumn(0, firstColumn);
        m.SetColumn(1, secondColumn);
        m.SetColumn(2, thirdColumn);
        m.SetColumn(3, fourthColumn);
        return m;
    }

    public static Matrix4x4 MatrixFromRows(Vector4 firstRow, Vector4 secondRow,
        Vector4 thirdRow, Vector4 fourthRow)
    {
        Matrix4x4 m = new Matrix4x4();
        m.SetRow(0, firstRow);
        m.SetRow(1, secondRow);
        m.SetRow(2, thirdRow);
        m.SetRow(3, fourthRow);
        return m;
    }
}
