﻿using System;
using System.Diagnostics;
using System.Threading;

public static class PerformanceTest
{  
    public static void TestSleep()
    {
        TestAction(() => Thread.Sleep(1), 1000);
    }

    public static void TestAction(Action a, int iterations)
    {
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();

        for (int i = 0; i < iterations; i++)
        {
            a();
        }

        stopwatch.Stop();

        UnityEngine.Debug.Log("Time elapsed: " + stopwatch.Elapsed.TotalMilliseconds + " for " + iterations + " iterations");
    }
}
