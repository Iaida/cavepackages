﻿// Utility class for controlled 'Random.State' when using random functions in combination with networking.
// Works with instances which wrap static 'Random' functions with 'WrappedRandomExecution' and keep track of their own 'Random.State'.
// Makes use of lazy evaluation for wrapping the function call without changing the global 'Random.State' immediately.

using UnityEngine;

public class RandomInstance
{
    private int _seed;
    
    private Random.State _state;

    public RandomInstance(int seed)
    {
        InitState(seed);
    }

    // used to replicate static 'Random' functions, but can also be used to
    // wrap entire algorithms which use multiple static 'Random' functions
    public T WrappedRandomExecution<T>(Lazy<T> randomFunction)
    {
        // save global state to temporary
        Random.State externalState = Random.state;
        // change global state to the current instance's state
        Random.state = _state;
        // delayed execution of 'randomFunction'
        T result = randomFunction.Value;
        // save new global state to current instance
        _state = Random.state;
        // restore global state from temporary
        Random.state = externalState;
        return result;
    }


    public Vector2 InsideUnitCircle()
    {
        return WrappedRandomExecution(new Lazy<Vector2>(() => Random.insideUnitCircle));
    }

    public Vector3 InsideUnitSphere()
    {
        return WrappedRandomExecution(new Lazy<Vector3>(() => Random.insideUnitSphere));
    }

    public Vector3 OnUnitSphere()
    {
        return WrappedRandomExecution(new Lazy<Vector3>(() => Random.onUnitSphere));
    }

    public Quaternion Rotation()
    {
        return WrappedRandomExecution(new Lazy<Quaternion>(() => Random.rotation));
    }

    public Quaternion RotationUniform()
    {
        return WrappedRandomExecution(new Lazy<Quaternion>(() => Random.rotationUniform));
    }

    public float Value()
    {
        return WrappedRandomExecution(new Lazy<float>(() => Random.value));
    }

    public void InitState(int seed)
    {
        // needs to use dummy 'object', because 'void' is not a valid template parameter in c#
        WrappedRandomExecution(new Lazy<object>(() =>
        {
            _seed = seed;
            Random.InitState(_seed);
            return new object();
        }));
    }

    public float Range(float min, float max)
    {
        return WrappedRandomExecution(new Lazy<float>(() => Random.Range(min, max)));
    }

    public int Range(int min, int max)
    {
        return WrappedRandomExecution(new Lazy<int>(() => Random.Range(min, max)));
    }

    public Vector3 PositionOnDisk(float radius, float diskHeight)
    {
        return WrappedRandomExecution(new Lazy<Vector3>(() => RandomUtilities.PositionOnDisk(radius, diskHeight)));
    }

    // takes values from 0 to 100
    public bool ChanceRoll(int percent)
    {
        return WrappedRandomExecution(new Lazy<bool>(() => RandomUtilities.ChanceRoll(percent)));
    }

    public bool RandomBool()
    {
        return WrappedRandomExecution(new Lazy<bool>(RandomUtilities.RandomBool));
    }
}
