﻿using UnityEngine;

public static class RandomUtilities
{
    // Alternative to unity's internal random seed generation, for example with particle systems
    public static uint GenerateSeed()
    {
        // based on: https://stackoverflow.com/questions/31451485/how-do-i-generate-a-random-uint-with-a-maximum/31451639#31451639
        int randomSignedInt = Random.Range(int.MinValue, int.MaxValue);
        return (uint)(randomSignedInt + (uint)int.MaxValue);
    }

    public static Vector3 PositionOnDisk(float radius, float diskHeight)
    {
        // http://mathworld.wolfram.com/DiskPointPicking.html
        //float angle = Random.Range(0.0f, 2 * Mathf.PI);
        //float radiusScaling = Random.Range(0.0f, 1.0f);
        //float x = Mathf.Cos(angle) * Mathf.Sqrt(radiusScaling) * radius;
        //float z = Mathf.Sin(angle) * Mathf.Sqrt(radiusScaling) * radius;
        //return new Vector3(x, diskHeight, z);

        // https://docs.unity3d.com/ScriptReference/Random-insideUnitCircle.html
        Vector2 twoDimPos = Random.insideUnitCircle * radius;
        return new Vector3(twoDimPos.x, diskHeight, twoDimPos.y);
    }

    // takes values from 0 to 100
    public static bool ChanceRoll(int percent)
    {
        return Random.Range(0, 100) < percent;
    }

    public static bool RandomBool()
    {
        return Random.Range(0, 2) == 1;
    }
}
