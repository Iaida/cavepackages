﻿using UnityEngine;


// Utility class which demonstrates the usage of a 'RangedFloat' variable to
// randomize volume and pitch of an audio source to make it sound less repetitive
[RequireComponent(typeof(AudioSource))]
public class RandomizeVolumePitch : MonoBehaviour
{
    [MinMaxRange(0, 1)]
    public RangedFloat VolumeRange;

    [MinMaxRange(0, 3)]
    public RangedFloat PitchRange;

    private AudioSource _audioSource;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        Randomize();
    }

    public void Randomize()
    {
        _audioSource.volume = VolumeRange.RandomValue;
        _audioSource.pitch = PitchRange.RandomValue;
    }
}
