﻿// from: https://www.youtube.com/watch?v=6vmRwLYWNRo

using System;
using Random = UnityEngine.Random;

[Serializable]
public struct RangedFloat
{
    public float MinValue;
    public float MaxValue;
    public float RandomValue
    {
        get { return Random.Range(MinValue, MaxValue); }
    }
}