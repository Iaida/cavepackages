﻿using UnityEngine;

// Utility script for scaling objects(which have their pivot point at the center) upwards while translating them upwards at the same time
// avoids clipping into the downwards direction, useful when scaling objects over time
public class ScaleUpwards : MonoBehaviour
{
    private Vector3 _worldFloorPosition;

    private void Start ()
	{
	    _worldFloorPosition = transform.position;
	}
	
    private void Update ()
	{
        // translate the object along the y-axis for half its y-scaling
	    transform.position = _worldFloorPosition + new Vector3(0, transform.localScale.y / 2, 0);
	}
}
