﻿using System;
using System.Globalization;

public static class StringUtilities
{
    // float decimal point parsing: https://stackoverflow.com/questions/1354924/how-do-i-parse-a-string-with-a-decimal-point-to-a-double/1354926#1354926
    public static float StringToFloat(string input)
    {
        return float.Parse(input, CultureInfo.InvariantCulture);
    }

    public static string[] Split(string input, string stringDelimiter)
    {
        return input.Split(new[] { stringDelimiter }, StringSplitOptions.RemoveEmptyEntries);
    }

    public static string[] Split(string input, char charDelimiter)
    {
        return input.Split(new[] { charDelimiter }, StringSplitOptions.RemoveEmptyEntries);
    }

    public static string[] Split(string input, char[] charDelimiters)
    {
        return input.Split(charDelimiters, StringSplitOptions.RemoveEmptyEntries);
    }

    public static string LastLine(string input)
    {
        if (string.IsNullOrEmpty(input) || input == "\n")
            return null;
        if (input.Length == 1)
            return input;

        // minimum length of 2 guaranteed from this point

        // last character is assumed to be '\n' and to be discarded
        int lastLineLastCharIndex = input.Length - 2;
        // LastIndexOf with 2 parameters searches from lastLineLastCharIndex to beginning
        int lastNewLineIndex = input.LastIndexOf('\n', lastLineLastCharIndex);
        // check if only one line
        return lastNewLineIndex == -1 
            ? input.Substring(0, lastLineLastCharIndex + 1) 
            : input.Substring(lastNewLineIndex + 1, lastLineLastCharIndex - lastNewLineIndex);
    }

    // returns first line without '\n' at the end
    public static string FirstLine(string input)
    {
        if (string.IsNullOrEmpty(input) || input == "\n")
            return null;

        int firstNewLineIndex = input.IndexOf('\n');
        return input.Substring(0, firstNewLineIndex);
    }
}
