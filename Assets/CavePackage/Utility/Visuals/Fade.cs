﻿using UnityEngine;

public class Fade : MonoBehaviour
{
    private enum FadeMode
    {
        Off, On, In, Out
    };

    [SerializeField]
    private FadeMode _currentMode = FadeMode.Off;

    private float _duration;
    private Texture2D _texture;
    private const int Depth = 1000;
    private float _alpha;
    private int _fadeDirection;

    private void Update()
    {
        if (_currentMode == FadeMode.Off || _currentMode == FadeMode.On)
            return;

        _alpha += _fadeDirection * (Time.deltaTime / _duration);
        _alpha = Mathf.Clamp01(_alpha);
    }

    private void OnGUI()
    {
        if (_currentMode == FadeMode.Off)
            return;
        if (_currentMode == FadeMode.In && Mathf.Approximately(_alpha, 0.0f))
            _currentMode = FadeMode.Off;
        if (_currentMode == FadeMode.Out && Mathf.Approximately(_alpha, 1.0f))
            _currentMode = FadeMode.On;

        Color newColor = GUI.color;
        newColor.a = _alpha;
        GUI.color = newColor;

        GUI.depth = Depth;

        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), _texture);
    }

    private static Texture2D GenerateSinglePixelTexture(Color color)
    {
        Texture2D tex = new Texture2D(1, 1);
        tex.SetPixel(0, 0, color);
        tex.Apply();
        return tex;
    }

    public void FadeIn(Color color, float duration)
    {
        _currentMode = FadeMode.In;
        _texture = GenerateSinglePixelTexture(color);
        _duration = duration;
        _fadeDirection = -1;
    }

    public void FadeOut(Color color, float duration)
    {
        _currentMode = FadeMode.Out;
        _texture = GenerateSinglePixelTexture(color);
        _duration = duration;
        _fadeDirection = 1;
    }

    public void Off()
    {
        _currentMode = FadeMode.Off;
        _alpha = 0.0f;
    }

    public void On(Color color)
    {
        _currentMode = FadeMode.On;
        _texture = GenerateSinglePixelTexture(color);
        _alpha = 1.0f;
    }


    // for debugging purposes, right click component in inspector
    [ContextMenu("FadeInTest")]
    private void FadeInTest()
    {
        FadeIn(Color.black, 3.0f);
    }

    [ContextMenu("FadeOutTest")]
    private void FadeOutTest()
    {
        FadeOut(Color.black, 3.0f);
    }

    [ContextMenu("OnTest")]
    private void OnTest()
    {
        On(Color.black);
    }

    [ContextMenu("OffTest")]
    private void OffTest()
    {
        Off();
    }
}
