﻿using UnityEngine;

// Utility script for taking pictures with cave camera without the game's colors changing the lighting

public class FlashScreen : MonoBehaviour
{
    private Texture2D _singlePixelTexture;
    private bool _flash;
    private Rect _rect;

    private void Start()
    {
        _flash = false;
        _singlePixelTexture = new Texture2D(1, 1);
        _singlePixelTexture.SetPixel(0, 0, Color.white);
        _singlePixelTexture.Apply();
        _rect = new Rect(0, 0, Screen.width, Screen.height);
    }

    public void FlashForSecs(float seconds)
    {
        _flash = true;
        Invoke("DeactivateFlash", seconds);
    }

    private void DeactivateFlash()
    {
        _flash = false;
    }

    public void OnGUI()
    {
        if (_flash)
        {
            // draws _singlePixelTexture's color over entire screen
            GUI.DrawTexture(_rect, _singlePixelTexture);
        }     
    }

    [ContextMenu("FlashTest")]
    private void FlashTest()
    {
        FlashForSecs(0.1f);
    }
}