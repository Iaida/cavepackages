﻿#pragma warning disable 0618 // UNet Deprecation warning disabled

using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Video;

// https://docs.unity3d.com/ScriptReference/Video.VideoPlayer.html
// https://docs.unity3d.com/Manual/VideoSources-FileCompatibility.html

// problems with large video files:
// - looping sometimes doesn't work, even with suggested workarounds
// - synchronizing by VideoPlayer's 'frame' variable results in stuttering


[RequireComponent(typeof(VideoPlayer))]
[NetworkSettings(channel = 0, sendInterval = 0)]
public class SyncVideo : NetworkBehaviour, IAllClientsConnected
{
    private VideoPlayer _videoPlayer;

    private void Awake()
    {
        // use component toggles: waitForFirstFrame, playOnAwake

        _videoPlayer = GetComponent<VideoPlayer>();
        _videoPlayer.skipOnDrop = true;
        //_videoPlayer.Prepare();
    }

    [ClientRpc]
    private void RpcRestart()
    {
        Restart();
    }

    private void RestartAll()
    {
        RpcRestart();
        Restart();
    }

    private void Restart()
    {
        _videoPlayer.Stop();
        _videoPlayer.Play();

        // necessary for large video looping
        // see: https://stackoverflow.com/questions/42805250/unity-5-6-videoplayer-larger-videos-does-not-leave-videoplayer-isplaying-loop
        //_videoPlayer.isLooping = true;
    }

    [ContextMenu("Restart")]
    public void OnAllClientsConnected()
    {
        RestartAll();
    }
}