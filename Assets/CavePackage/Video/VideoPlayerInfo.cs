﻿using UnityEngine;
using UnityEngine.Video;

// utility script for displaying various variables of a VideoPlayer component in inspector for debugging purposes
[RequireComponent(typeof(VideoPlayer))]
public class VideoPlayerInfo : MonoBehaviour
{
    private VideoPlayer _videoPlayer;

    public long Frame;
    public float FrameRate;
    public double Time;
    public bool SkipOnDrop;
    public bool ShowOnGui;

    private void Awake()
    {
        _videoPlayer = GetComponent<VideoPlayer>();
    }

    private void Start ()
    {
        FrameRate = _videoPlayer.frameRate;
        SkipOnDrop = _videoPlayer.canSetSkipOnDrop;
    }

    
    private void LateUpdate ()
    {
        Frame = _videoPlayer.frame;
        Time = _videoPlayer.time;
    }

    private void OnGUI()
    {
        if (ShowOnGui)
        {
            int w = Screen.width, h = Screen.height;

            GUIStyle style = new GUIStyle();

            Rect rect = new Rect(0, 0, w, h * 0.02f);
            style.alignment = TextAnchor.UpperRight;
            style.fontSize = h * 2 / 100;
            style.normal.textColor = Color.yellow;

            string text = string.Format("{0:0.0}s", _videoPlayer.time);

            GUI.Label(rect, text, style);
        }
    }
}
