# Getting Started
* **Unity Version**: The Package does not work with Unity version 2019.1 or newer, please use Unity 2018.2.21, 2018.3 or 2018.4 LTS

* **Download**: The Cavepackage can be downloaded [[here|https://vr-tools.fh-wedel.de:3000/fha/CavePackage/raw/master/CavePackage%202018.2.21.unitypackage]]. Please send your questions to fha@fh-wedel.de

* **Importing Custom Packages**: Assets -> Import Package -> Custom Package -> Choose .unitypackage file

* After importing the package, an additional **menu bar** named **CavePackage** will appear in the Unity editor.

## Making a scene Cave-deployable
* **Adding relevant prefabs**: CavePackage -> Cave Setup
* **Non-headless Server**: CavePackage -> Assign Local IP to Server IP

## Build
* File -> Build Settings
* Add scenes, use `Add Open Scene` if necessary
* **Target Platform**: Linux
* **Architecture**: x86_64
* **Development Build**: optional, can be used for enabling further options or to display errors in a window at the bottom of the screen
* **Autoconnect Profiler**: optional, used for [remote profiling](https://docs.unity3d.com/Manual/ProfilerWindow.html)
* **Script Debugging**: optional, allows debugging with a game running in the Unity Player, see: [Debugging C# code in Unity
](https://docs.unity3d.com/Manual/ManagedCodeDebugging.html)
* **Headless Mode**: optional, build game for server use and without visual elements


