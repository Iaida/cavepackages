# UNet Deprecation
* Package does not work with Unity version 2019.1 or newer, please use Unity 2018.2.21, 2018.3 or 2018.4 LTS
* [New Networking Page](https://unity.com/solutions/real-time-multiplayer/network-transport-layer)
* New Networking Library is still in Alpha: [Real-time Multiplayer Alpha repository](https://github.com/Unity-Technologies/multiplayer)
* [FPS Sample](https://unity.com/fps-sample)
* [FPS Sample repository](https://github.com/Unity-Technologies/FPSSample)
* [Connected Games: Unity's new networking foundation - Unite LA](https://www.youtube.com/watch?v=EsUGX0FZi5k)
* [Deep dive into networking for Unity's FPS Sample game - Unite LA](https://www.youtube.com/watch?v=k6JTaFE7SYI)

# CavePackage Menu Bar
* `Cave Setup`: Applies `Disable Resolution Dialog`, `Add CaveManagement to Scene` and `Add CaveCube to Scene`
* `Add CaveManagement to Scene`: Adds the `CaveManagement` prefab to the current scene
* `Add CaveCube to Scene`: Adds the `CaveCube ` prefab to the current scene. Contains Cave faces to get a sense of scale within a scene. Contains tracker object such as the wristbands and the WiiMote.
* `Disable Resolution Dialog`: Disables the resolution dialog that normally appears when starting a unity program. Has to be deactivated for the cave client, where the dialog can not be skipped without the proper input devices.
* `Assign Local IP to Server IP`: Assigns the IP of the computer, which currently runs the editor, as the server IP. Not necessary when building for headless mode.
* `Assign Cave Near and Far Plane Values to Selected Camera`: Only works when a camera object is selected in the editor. Takes the Settings' near and far plane values and assigns them to the current camera. This allows for easier testing inside the editor, for example when placing objects in the distance, which should still be visible when playing inside the Cave.

# Scenes
* `360 mono Video`: Synced Roller Coaster 4K 360 degree video, no stereo effect
* `Cinema`: Single plane in the distance with a video texture
* `CurveTest`: Testing of functionality from `Utility/Curve` folder
* `EventTest`: Testing of functionality from `Event` folder
* `GridTest`: copy of `Testbild` cave project without sound test
* `MovementComparison`: Testing of functionality from `Movement` folder, show difference in smoothness
* `Multiplayer`: Testing of `Command` functionality with additional clients
* `ObjectPoolTest`: Testing of functionality from `ObjectPooling` folder
* `Piano`: Testing of functionality from `Audio` folder
* `ProjectilePhysics`: Testing of functionality from `Projectile Physics` folder. Might display warning `Kinematic body only supports Speculative Continuous collision` in some versions. Make sure every spawned prefab also uses `Speculative Continuous collision`
* `SpaceTransformationTest`: Testing of the cave-specific projection matrix when coupled with additional space transformations, which are applied to the camera. Shows that any parent or grandparent transforms do not disturb the orientation of the camera or the `CaveCube` prefab.
* `Vertigo`: Wooden plank with fire particles underneath. Plank placed to fit the cave floor.

# Subfolders

## Audio
* Contains various scripts for processing of audio files
* Can be helpful for procedural generation based on audio input
* Used in `Piano` scene

## CorePrefabs
* Contains the `CaveManagement` prefab, which is necessary in a scene when creating a Cave application
* Contains the `CaveCube` prefab, which consists of the cave screen planes according to their real world dimensions and the different tracked objects, such as the WiiMote and wristbands

## EffectExamples
* Contains selected Particle Systems from the official Unity Particle Packages
* Used in `Vertigo` scene

## Events
* Contains scripts, which demonstrate how one can build their own event-system with the help of ScriptableObjects
* Used in `EventTest` scene

## Input
* Contains various scripts for different input methods
* Contains scripts, which manage the tracking data and transforms them into unity's coordinate system
* Contains scripts for managing the communication with the wii-server, including WiiMote and BalanceBoard
* Contains the `CustomBalanceBoard` and `DancePad` scripts, which treat these devices like USB-connected joysticks

## Movement
* Contains scripts, which show the different methods for synchronizing data over the network
* Used in `MovementComparison` scene, which shows the difference in fluidity when interpolating or directly setting the position

## Network
* Contains scripts and prefabs for managing the cave network
* Additional non-cave clients are allowed to connect to the server when increasing the `CaveNetworkManager's` `NumberOfAdditionalClients` value 
* The scripts `CommandUsage` and `SpawnAuthority` show how one could send `Commands` from a single additional client to the server, which then gets distributed to all clients
* Contains a `Player` prefab, which gets instantiated for each connected client
* Contains a `CommandSpawn` prefab, which can be instantiated by a `Player` with the `SpawnAuthority` component, which allows the object to send out `Commands`
* `CaveMode`: Contains the current mode for the game instance. Used to differentiate between server, cave instances and additional Clients for Mulitplayer purposes.

## ObjectPooling
* Contains scripts and a prefab to demonstrate the concept of Object Pooling in combination with instantiating objects over a network
* Used in `ObjectPoolTest` and other scenes, such as `Vertigo` to spawn many particle systems

## Particle Systems
* Contains a `SyncParticleSystems` script, which synchronizes particle systems over the network via restarts. This is especially important and noticable, when a particle systems spans multiple screens within the cave
* Contains a `FlamePlacementSync` script, which is used in the `Vertigo` scene to spawn multiple particle systems

## Projectile Physics
* Contains scripts and prefabs for the scene `ProjectilePhysics`, which uses unity's physics system and the tracking of the WiiMote to implement a Lightsaber training simulator

## Rendering
* Contains various scripts for the cave-specific stereo projection and debugging tools
* The `CaveFaceRendering` component has 4 `Renderer` references, which can be assigned to the different sides of the cave from the `CaveCube` -> `CaveCubeModel` prefab. Then, the textures on the cube are replaced with RenderTextures, which simulate the projection onto the different cave screens depending on the camera position. This can be helpful when debugging the projection matrix without the cave.

## Settings
* Contains the `CaveSettings` Script and ScriptableObject instance, which contain the various settings for the cave, such as ip addresses, ports and rendering
* `CommandLineOptions`: Relays command line parameters to the `CaveSettings` and `CaveMode` scripts
* `CaveSettings'` `ParseSettingsMember` function allows for changes to each member via the command line parameter such as changing the `ServerIp`

## Utility
* Contains various utility scripts, that are used throughout the package or were created for the use within specific projects
* `FreelookCamera`: Default camera script for movement with mouse and keyboard
* `HighScoreList`: Provides persistent Highscore Management
* `CircularBuffer`: Provides a Circular(Ring) Buffer implementation, which is used in the `TrackingInputAsync` script.
* `CurveUtilities`: Provides functions for curve interpolations, such as Spline and Bezier. Used in `CurveTest` scene.
* `CavePackageMenuBar`: Adds the `CavePackage` bar to the Unity editor to simplify the process of converting an existing project to a cave project
* `RandomInstance`: Can be used to create separate `Random` instances instead of the single global instance used by Unity.
* `RangedFloat`: Allow the definition of a float range between two values, which can be changed with a slider within the  inspector. Used in `RandomizeVolumePitch`.
* `Fade` and `FlashScreen`: Can be used to fade or instantly set the screen to a single color

## Video
* Cave Clients run on Linux and only support few video formats: [Video file compatibility
](https://docs.unity3d.com/Manual/VideoSources-FileCompatibility.html)
* Video files can be converted to the proper format with tools such as [FFmpeg](https://ffmpeg.org/)
* Contains texture and materials for rendering videos
* `SyncVideo`: restarts a video when all clients are connected to the server for synchronization
* Used in `360 mono Video` and `Cinema` scenes

# Debug with Single Local Cave Client
Start the standalone build over command line / powershell with the additional parameter `-server` or `-front`  
Alternative:  
1. Select `Cavemanagement's` child object `Network`  
2. Assign `Current Mode` to `Server`/`Front` in `Cave Mode` component  
3. Activate `Debug Single Local Cave Client` in `CaveNetworkManager` component  
4. Build standalone  
5. Assign `Current Mode`  to `Front`/`Server`  
6. Start Standalone executable and press Play in the Unity editor

## Visual error
* floor rendering nothing when no glasses tracked, Unity Console error: `matrix isvalid error for camera set proj matrix`  
Cause: happens when default camera height == floor height(0)  
Solution: prefer other default camera position, for example (0, 1.25, -1.25)

# Output to stdout
* Start standalone executable with parameter `-logFile /dev/stdout`

# OnApplicationQuit
* Does not get called on Linux(Cave Clients), see [[here|https://sdumetz.github.io/2017/07/01/handle-unix-signals-unity.html]] for an explanation and a possible workaround



# Old Unity Networking
* Unity connection to server running on windows might be blocked by windows firewall  
* [Manual: NetworkBehaviour callbacks](https://docs.unity3d.com/Manual/NetworkBehaviourCallbacks.html)
* [ScriptReference: NetworkClient.RegisterHandler](https://docs.unity3d.com/ScriptReference/Networking.NetworkClient.RegisterHandler.html)
* [Manual: UNetManager](https://docs.unity3d.com/Manual/UNetManager.html)
* [Manual: NetworkIdentity](https://docs.unity3d.com/Manual/class-NetworkIdentity.html)
* [ScriptReference: NetworkIdentity](https://docs.unity3d.com/ScriptReference/Networking.NetworkIdentity.html)
* [Manual: UNetActions](https://docs.unity3d.com/Manual/UNetActions.html)
* [Manual: UNetStateSync](https://docs.unity3d.com/Manual/UNetStateSync.html)
* [Manual: UNetAuthority](https://docs.unity3d.com/Manual/UNetAuthority.html)
* Network Information at the bottom of the Inspector in the Preview Window shows the properties `isServer`, `isClient`, `isLocalPlayer` and `hasAuthority`

## Separate client code from server code with attributes
* [ScriptReference: ServerCallbackAttribute](https://docs.unity3d.com/ScriptReference/Networking.ServerCallbackAttribute.html)
* [ScriptReference: ClientRpcAttribute](https://docs.unity3d.com/ScriptReference/Networking.ClientRpcAttribute.html)
* [ScriptReference: TargetRpcAttribute](https://docs.unity3d.com/ScriptReference/Networking.TargetRpcAttribute.html)

## Spawning Objects over Network
* [Tutorial: handling non-player objects](https://unity3d.com/de/learn/tutorials/topics/multiplayer-networking/handling-non-player-objects)
* [Manual: UNetSpawning](https://docs.unity3d.com/Manual/UNetSpawning.html)
* [ScriptReference: NetworkManager.spawnPrefabs](https://docs.unity3d.com/ScriptReference/Networking.NetworkManager-spawnPrefabs.html)
* [ScriptReference: ClientScene.RegisterPrefab](https://docs.unity3d.com/ScriptReference/Networking.ClientScene.RegisterPrefab.html)
* [ScriptReference: NetworkServer.Spawn](https://docs.unity3d.com/ScriptReference/Networking.NetworkServer.Spawn.html)
* [ScriptReference: NetworkServer.Destroy](https://docs.unity3d.com/ScriptReference/Networking.NetworkServer.Destroy.html)
* [ScriptReference: NetworkServer.UnSpawn](https://docs.unity3d.com/ScriptReference/Networking.NetworkServer.UnSpawn.html)
* [Manual: UNetCustomSpawning](https://docs.unity3d.com/Manual/UNetCustomSpawning.html)

## Network Channels
* [ScriptReference: NetworkSettingsAttribute.channel](https://docs.unity3d.com/ScriptReference/Networking.NetworkSettingsAttribute-channel.html)
* [ScriptReference: QosType](https://docs.unity3d.com/ScriptReference/Networking.QosType.html)
* [ScriptReference: Channels](https://docs.unity3d.com/ScriptReference/Networking.Channels.html)
* [ScriptReference: NetworkManager.channels](https://docs.unity3d.com/ScriptReference/Networking.NetworkManager-channels.html)


# Scripting Runtime Version
* .Net 3.5 is marked deprecated since 2018.3 and .Net 4.x Equivalent is the new default for new projects
* When building a linux project on the network drive for .Net 4.x the following build error occurs: `ArgumentException: UNC paths should be of the form \\server\share. System.IO.Path.InsecureGetFullPath (System.String path)`  
**Workaround**: Treat the network drive like a local drive (should look like: `share(\\\\VR_TOOLS) (Z:)`) by right clicking the network drive in the explorer and build to this new path